#ifndef SIMPLE_ENGINE_H
#define SIMPLE_ENGINE_H

#include "cens_engine.h"

using namespace cens;

/**
 * @brief A CENSEngine - derived simple class
 */
class SimpleEngine : public CENSEngine {   
    public:   

        SimpleEngine():time_counter(0) {}

        // overloadings
        void initObjects();
        void step( int timestep);

    private: 

        // a rigid body
        btRigidBody* aBody; 
        // a counter of the simulation steps
        int time_counter; 
};

#endif //SIMPLE_ENGINE_H


