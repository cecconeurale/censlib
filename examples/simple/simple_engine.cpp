#include "simple_engine.h"

using namespace cens;


// define the initObjects function
void SimpleEngine::initObjects() {

    // Create a simple cube

    // Create a 10x10x10 box shape 
    // ( giving the half-extents of dimensions ) 
    btCollisionShape* shape = 
        new btBoxShape( btVector3(5,5,5) );	

    // Create a transform
    btTransform transform;

    // Initialize the transform
    // to the  world origin, with no rotation
    transform.setIdentity();
    transform.setOrigin(btVector3(0,0,0));

    // Create the body giving mass, 
    // shape and transform.
    aBody = 
        localCreateRigidBody(
                1.0, // mass
                transform, // transform 
                shape); // shape
}



// define the step function
// in which we apply a force to the box
// for the first 10 steps of the simulation
void SimpleEngine::step( int timestep ) 
{

    // apply a force on the cube

    // transform of the body
    btTransform &aBodyTransform = aBody->getWorldTransform();
    // direction of the force
    btVector3 direction = btVector3(.5,.5,1);
    // magnitude of the force
    float force = 20;

    // applying the force
    if (time_counter<10)
        aBody->applyCentralForce(
                // multiplying the direction times the transform
                // the local reference to the object is mantained
                aBodyTransform * direction * force );

    // if more tha 200 steps are been run
    // end the simulation 
    if( time_counter > 200 )
        quit();

    // update a counter to know in 
    // which part of the simulation we are
    time_counter++;

    // This call should always be here. 
    // Without it bullet and graphics steps 
    // are not called, and the loop of 
    // simulation doesn't go on.
    CENSEngine::step(timestep);

}


