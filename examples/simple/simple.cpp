#include "simple_engine.h"

using namespace cens;

int main(int argc, char** argv) 
{
    SimpleEngine engine;
    engine.init(argc,argv);

    engine.run();

    //// If you need step control
    //// use stepRun : 
    // for(int t=0;t<200; t++)
    // {
    //     engine.stepRun();
    // }   
    

    return 0;
}


