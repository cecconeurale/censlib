#include "engine.h"

using namespace cens;


// Then define the initObjects function
// in which we initialize the rigid body
// as a box
void Engine::initObjects() {
   
    // Simply import all objects form a .bullet file
    m_eRobots["robot_test"] = loadBulletFile("robot_test","test.bullet");
    
    // Object3 is a touch sensor
    addTouchSensor(m_eBodies["Object3"],"Object3");
    
    // Object3 is red
    m_eShapes[m_eBodies["Object3"]]->setColor(Vector3f(1,0,0));
   
    toggleTexture();
    toggleJointAxes();
}

// Finally, define the step function
// in which we apply a force to the box
// for the first 10 steps of the simulation
void Engine::step( int timestep ) 
{
 
   
    static double angle = 0; 
    static double impulse = 1; 
    angle -= M_PI/100.;
   

    saveCameraPixelMap(0, timecounter,"png");

    // if Object3 touches something
    if(m_eSensors["Object3"]->isTouched())
    {     
        // go backward 
        angle += M_PI/10.;
        
        // std::cout <<  
            // impulse << " " <<
            // m_eSensors["Object3"]->getPressure() << std::endl;
         
        // higher impulse
        impulse += .01;

    } 

    m_eRobots["robot_test"]->move("Object2_to_Object1", angle, impulse);
    m_eRobots["robot_test"]->move("Object3_to_Object2", angle, impulse);
    
    // if(timecounter>200)
        // exit(0);

    timecounter++;

    // This call should always be here. 
    // Without it bullet and graphics steps 
    // are not called, and the loop of 
    // simulation doesn't go on.
    CENSSerializedEngine::step(timestep);

}


