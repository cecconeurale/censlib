#include "engine.h"


int main(int argc, char** argv) 
{
    Engine engine;
    engine.init(argc,argv);

    engine.run();

    return 0;
}


