#ifndef ENGINE_H
#define ENGINE_H

#include "cens_engine.h"

using namespace cens;


class Engine : public CENSEngine {   
    public:   
        Engine():time_counter(0) {}
        void initObjects();
        void step( int timestep);
    private: 
        btRigidBody* body; // a rigid body
        int time_counter; // a counter of the simulation steps
};

#endif //ENGINE_H

