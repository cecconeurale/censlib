#include "engine.h"

using namespace cens;



// Define the initObjects function
// in which we initialize the rigid body
// as a box
void Engine::initObjects() {

    // A SPHERE
    // Create a Sphere shape 
    // with radius 2
    btCollisionShape* spShape =  
        new btSphereShape(
                btScalar(2.) );

    // Create a transform
    btTransform spTransform;
    // Initialize the transform to (0,3,8), 
    // with no rotation
    spTransform.setIdentity();
    spTransform.setOrigin(btVector3(0,0,8)); 
    spTransform.setRotation(btQuaternion(0,0,0));

    // Create the soft body giving mass and shape.
    btSoftBody *softSphere =localCreateSoftBody(
            btScalar(10), 
            spShape); 
    // give the sphere the righttransform
    softSphere->transform( spTransform );

    // Linear stiffness [0,1]
    softSphere->m_materials[0]->m_kLST   =   0.1f;  
    // Area/angular stiffness [0,1]
    softSphere->m_materials[0]->m_kAST   =   0.1f; 
    // volume stiffness [0,1]
    softSphere->m_materials[0]->m_kVST   =   0.1f;

    softSphere->setPose( true, false );
    softSphere->generateBendingConstraints(3);


    // Create a 10x10x10 box shape 
    // ( giving the half-extents of dimensions ) 
    btCollisionShape* shape = 
        new btBoxShape( btVector3(15,15,2) );	

    // Create a transform
    btTransform transform;

    // Initialize the transform to the origin, 
    // with no rotation
    transform.setIdentity();
    transform.setOrigin(btVector3(0,0,0));

    // Create the body giving mass, shape and transform.
    body = 
        localCreateRigidBody(
                0.0, // mass
                transform, // transform 
                shape); // shape
}

// Finally, define the step function
// in which we apply a force to the box
// for the first 10 steps of the simulation
void Engine::step( int timestep ) 
{

    // applying a force on the body

    // thetransform of the body
    btTransform &aBodyTransform = body->getWorldTransform();
    // the direction of the force
    btVector3 direction = btVector3(.5,.5,1);
    // the magnitude of the force
    float force = 20;

    // applying the force
    if (time_counter<10)
        body->applyCentralForce(
                // multiplying the direction times the transform
                // the local reference to the object is mantained
                aBodyTransform * direction * force );

    // if more tha 200 steps are been run
    // end the simulation 
    if( time_counter > 200 )
        quit();

    // update a counter to know in 
    // which part of the simulation we are
    time_counter++;

    // This call should always be here. 
    // Without it bullet and graphics steps 
    // are not called, and the loop of 
    // simulation doesn't go on.
    CENSEngine::step(timestep);

}

