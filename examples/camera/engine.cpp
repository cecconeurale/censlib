#include "engine.h"

void Engine::initObjects() {
    
    // Simply import all objects form a .bullet file
    m_eRobots["robot_test"] = 
        loadBulletFile("robot_test","test.bullet");

    btTransform local;
    local.setOrigin(btVector3(0,0,-1));
    local.setRotation(btQuaternion(M_PI/2.,0,0));

    addCamera( 
            "SphereCamera",
            m_eBodies["Sphere"], // pointer to the attached body 
            local,  // local transform relative to the attached body 
            "Sphere_EYE", // name of the camera 
            480,400, // dimensions of the camera window (pixels)
            800,50, // position of the camera window on the screen (pixels) 
            btVector3(0,0,1),  // UP vector of the camera
            10, // target distance
            100,
            .00001); 

    toggleTexture();
    toggleCameraAxis();
}

void Engine::step( int timestep ) 
{

    // This call should always be here. 
    // Without it bullet and graphics steps 
    // are not called, and the loop of 
    // simulation doesn't go on.
    CENSEngine::step(timestep);
}

