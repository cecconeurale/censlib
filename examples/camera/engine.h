#ifndef ENGINE_H
#define ENGINE_H

#include "cens_serialized_engine.h"


using namespace cens;


class Engine : public CENSSerializedEngine {

  public:

    void initObjects();
    void step( int timestep);


};

#endif // ENGINE_H
