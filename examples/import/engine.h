#ifndef ENGINE_H
#define ENGINE_H

#include "cens_serialized_engine.h"

using namespace cens;


// First, declare a CENSEngine derived class
class Engine : public CENSSerializedEngine {   
    public:   
        void initObjects();
        void step( int timestep);
        
};

#endif //ENGINE_H

