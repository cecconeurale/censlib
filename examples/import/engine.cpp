#include "engine.h"

using namespace cens;


// Then define the initObjects function
// in which we initialize the rigid body
// as a box
void Engine::initObjects() {
   
    // Simply import all objects form a *.bullet file
    m_eRobots["robot_test"]=loadBulletFile("robot_test","test.bullet");
}

// Finally, define the step function
// in which we apply a force to the box
// for the first 10 steps of the simulation
void Engine::step( int timestep ) 
{
    // This call should always be here. 
    // Without it bullet and graphics steps 
    // are not called, and the loop of 
    // simulation doesn't go on.
    CENSEngine::step(timestep);

}

