include_directories( ${INCLUDES} )

add_executable(import 
    import.cpp
    engine.cpp
    )

target_link_libraries ( import  
    ${LIBS}
    CENS_Static_Lib
     ) 

add_custom_command(TARGET import PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                   ${CMAKE_CURRENT_SOURCE_DIR}/parameters $<TARGET_FILE_DIR:import>/parameters )

 add_custom_command(TARGET import PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy
                   ${CMAKE_CURRENT_SOURCE_DIR}/test.bullet $<TARGET_FILE_DIR:import> )


