#include "rat_engine.h"

using namespace cens;

int main(int argc, char** argv) 
{
    RatEngine engine;
    engine.init(argc,argv);

    engine.run();

    return 0;
}


