#ifndef MY_ENGINE_H
#define MY_ENGINE_H

#include "cens_serialized_engine.h"

using namespace cens;


// First, declare a CENSEngine derived class
class RatEngine : public CENSSerializedEngine {   
    public:   
        void initObjects();
        void step( int timestep);
};

#endif //MY_ENGINE_H

