include_directories( ${INCLUDES} )

add_executable( rat 
    rat.cpp
    rat_engine.cpp
    )

target_link_libraries ( rat  
    ${LIBS}
    CENS_Static_Lib
     ) 

 add_custom_command(TARGET rat PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                   ${CMAKE_CURRENT_SOURCE_DIR}/parameters $<TARGET_FILE_DIR:rat>/parameters )

 add_custom_command(TARGET rat PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy
                   ${CMAKE_CURRENT_SOURCE_DIR}/test.bullet $<TARGET_FILE_DIR:rat> )


