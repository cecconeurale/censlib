#include "engine.h"

void Engine::initObjects() {

    //////////////////////////////////////////////////////
    // Initialize a sphere and add an hinge attached    //
    // to the world .                                   //     
    // On runtime a force is applied to the sphere.     //
    // As a result the sphere rotates around the hinge. //
    //////////////////////////////////////////////////////
     
    
    // initializing a sphere
    btCollisionShape* spShape =  
        new btSphereShape(
                btScalar(2.) );	
    btTransform spTransform;
    spTransform.setIdentity();
    spTransform.setOrigin(btVector3(4,4,2)); 
    spTransform.setRotation(btQuaternion(0,0,0));
    body = 
        localCreateRigidBody(
                btScalar(1), 
                spTransform, 
                spShape,
                btVector3(.6,.6,.8) );

    
    //initializing a joint.
    // set the ransform information for a joint: 
    btTransform jointTransform;
    jointTransform.setIdentity();

    // translation: the point in the solid body that is 
    //              constrained to the hinge. Coordinates are local,
    //              relative to the body.
    jointTransform.setOrigin( btVector3(-4, -4, 0)); 

    // rotation:    which axis the plane of the hinge constraint 
    //              will rotate on and how much it will rotate.
    jointTransform.setRotation(
            btQuaternion(
                btVector3(0,0,1), // axis
                0)); // angle

    // build the joint. Giving a single body 
    // and a single transform is enough to tell bullet 
    // to use the world as the other body.
    btHingeConstraint *constraint =  new btHingeConstraint(
            *body,
            jointTransform);

    // add the joint to the world
    // (argument true means they can compenetrate!) 
    m_phDynamicsWorld->addConstraint(constraint,true);

    toggleTexture();
    toggleObjectAxes();
    toggleJointAxes();
}



void Engine::step( int timestep ) 
{

    // A force is applied to the sphere.
    float force = 50;
    body->applyCentralForce(
            body->getWorldTransform() *   // body center of mass
            (btVector3(.1,0,0) * force) ); // direction of force
    // initialize a counter to know in 
    // which part of the simulation we are
    time_counter++;

    // This call should always be here. 
    // Without it bullet and graphics steps 
    // are not called, and the loop of 
    // simulation doesn't go on.
    CENSEngine::step(timestep);

}

