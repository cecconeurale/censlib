// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_graphics_shape.cpp
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#include "cens_graphics_shape.h"
#include "cens_types.h"

namespace cens 
{

    Vector3f GS_NULL_COLOR = Vector3f(-1,-1,-1);


    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::copyimage(const GLubyte *source) 
    {

        if(m_gsTextureImage)
            delete[] m_gsTextureImage;

        if(source==0) 
        {

            m_gsTextureWidth = 256;
            m_gsTextureHeight = 256;
            m_gsTextureImage = new GLubyte[256*256*3];
            for(int y=0;y<256;++y)
            {
                const int	t=y>>4;
                GLubyte*pi=m_gsTextureImage+y*256*3;
                for(int x=0;x<256;++x)
                {
                    const int	s=x>>4;
                    const GLubyte	b=180;
                    GLubyte	c = b+(((s+t)&1)&1)*(255-b);
                    pi[0]=pi[1]=pi[2]=c;pi+=3;
                }
            }

        } else 
        {

            m_gsTextureImage = new GLubyte[m_gsTextureWidth*m_gsTextureHeight*3];
            for(int x=0;x<m_gsTextureWidth*m_gsTextureHeight*3; x++) 
            {
                m_gsTextureImage[x] = source[x];
            }

        }

    }


    ////////////////////////////////////////////////////////////////////////////////

    CENSGraphicsShape::~CENSGraphicsShape() 
    {

        if(m_gsTextureImage)
            delete[] m_gsTextureImage;

    }

    ////////////////////////////////////////////////////////////////////////////////
    // BUILDS //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::buildAsPlane(const Vector3f &orig, const Vector3f &v1,
            const Vector3f &v2, const Vector3f &color, CENSPixelMap texture) 
    {

        m_gsType = GS_PLANE;
        m_gsColor=color;
        m_gsTextureWidth = texture.getWidth();
        m_gsTextureHeight = texture.getHeight();
        copyimage(texture.getData());
        addVertex(orig);
        addVertex(v1);
        addVertex(v2);
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::buildAsBox(const Vector3f &halfExtents,
            const Vector3f &color, CENSPixelMap texture)
    {

        m_gsType = GS_BOX;
        m_gsColor=color;
        m_gsTextureWidth = texture.getWidth();
        m_gsTextureHeight = texture.getHeight();
        copyimage(texture.getData());
        // triples of texture coordinates for each triangle

        static int tex_coords[12][6] = 
        {
            {1,1,0,1,1,0},
            {0,0,1,0,0,1},
            {1,1,0,1,1,0},
            {1,0,0,1,0,0},
            {1,1,0,1,1,0},
            {1,0,0,1,0,0},
            {1,1,0,1,0,0},
            {0,0,1,1,1,0},
            {1,0,1,1,0,0},
            {0,0,1,1,1,0},
            {1,0,0,1,0,0},
            {0,1,1,1,1,0} };


        // defining half extents

        Vector3f dx(1,0,0);
        Vector3f dy(0,1,0);
        Vector3f dz(0,0,1);

        dx *= halfExtents[0];
        dy *= halfExtents[1];
        dz *= halfExtents[2];

        // defining vertices coordinates

        Vertices vertices;
        vertices.push_back( dx+dy+dz);
        vertices.push_back(-dx+dy+dz);
        vertices.push_back( dx-dy+dz);
        vertices.push_back(-dx-dy+dz);
        vertices.push_back( dx+dy-dz);
        vertices.push_back(-dx+dy-dz);
        vertices.push_back( dx-dy-dz);
        vertices.push_back(-dx-dy-dz);

        setVertices(vertices);

        for(int x=0; x<12; x++) 
        {
            addTexCoord(Vector2f(
                        tex_coords[x][0],
                        tex_coords[x][1] ));
            addTexCoord(Vector2f(
                        tex_coords[x][2],
                        tex_coords[x][3] ));
            addTexCoord(Vector2f(
                        tex_coords[x][4],
                        tex_coords[x][5] ));
        }

    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::buildAsSphere(float radius,
            const Vector3f &color, CENSPixelMap texture)
    {

        m_gsType = GS_SPHERE;
        m_gsColor=color;
        m_gsTextureWidth = texture.getWidth();
        m_gsTextureHeight = texture.getHeight();
        copyimage(texture.getData());

        float xtag = 1/(float)GS_SPHERE_LATS;
        float ytag = 1/(float)GS_SPHERE_LONGS;

        for(int i = 0; i <= GS_SPHERE_LATS; i++) 
        {

            float lat0 = M_PI * (-float(0.5) + (i - 1) * xtag);
            float z0 = radius*sin(lat0);
            float zr0 = radius*cos(lat0);

            float lat1 = M_PI * (-float(0.5) + i * xtag);
            float z1 = radius*sin(lat1);
            float zr1 = radius*cos(lat1);

            for(int j = 0; j <= GS_SPHERE_LONGS; j++) 
            {

                float lng = 2 * M_PI * (j - 1) * ytag;
                float x = cos(lng);
                float y = sin(lng);

                Vector3f ver1(x * zr1, y * zr1, z1);
                Vector3f ver2(x * zr0, y * zr0, z0);

                addTexCoord(Vector2f(xtag*i, ytag*j));
                addTexCoord(Vector2f(xtag*(i-1), ytag*j));
                addVertex(ver1);
                addVertex(ver2);
            }
        }

    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::buildAsConvex( 
            const Vertices &vtx,
            const unsigned int *idx, 
            int nvtxs, int nidxs, int ntrns,
            const TexCoords &texCoords, 
            const Vector3f &color, 
            CENSPixelMap texture)
    {

        m_gsType = GS_CONVEX;
        m_gsColor=color;
        m_gsTextureWidth = texture.getWidth();
        m_gsTextureHeight = texture.getHeight();
        copyimage(texture.getData());

        if (ntrns > 0)
        {
            int index = 0;

            for (int i = 0; i < ntrns; i++)
            {

                int i1 = index++;
                int i2 = index++;
                int i3 = index++;

                assert(i1 < nidxs &&
                        i2 < nidxs
                        && i3 < nidxs);

                int index1 = idx[i1];
                int index2 = idx[i2];
                int index3 = idx[i3];
                assert(index1 < nvtxs &&
                        index2 < nvtxs &&
                        index3 < nvtxs );

                Vector3f v1 = vtx[index1];
                Vector3f v2 = vtx[index2];
                Vector3f v3 = vtx[index3];

                Vector3f axis = (v3-v1).cross(v2-v1);
                axis.normalize ();

                m_gsVertices.push_back(v1);
                m_gsVertices.push_back(v2);
                m_gsVertices.push_back(v3);

                if(texCoords.size()==0) 
                {
                    float div = 10.;

                    m_gsTexCoords.push_back(Vector2f(
                                v1.x()/div, v1.z()/div ));
                    m_gsTexCoords.push_back(Vector2f(
                                v2.x()/div, v2.z()/div ));
                    m_gsTexCoords.push_back(Vector2f(
                                v3.x()/div, v3.z()/div ));
                } 
            }

            if(texCoords.size()!=0) 
            {
                setTexCoords(texCoords);
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::buildAsSoft(
            int numTriangles, 
            const TexCoords &texCoords, 
            const Vector3f &color, 
            CENSPixelMap texture)
    {

        m_gsType = GS_SOFT;
        m_gsColor=color;
        m_gsTextureWidth = texture.getWidth();
        m_gsTextureHeight = texture.getHeight();
        copyimage(texture.getData());

        m_gsVertices.resize(numTriangles*3);
        m_gsNormals.resize(numTriangles);

        if(texCoords.size()!=0) 
        {
            setTexCoords(texCoords);
        }

    }

    ////////////////////////////////////////////////////////////////////////////////
    // DRAW ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////


    void CENSGraphicsShape::draw(const Vector3f &org, const Matrix3f &rot)
    {
        static int indices[12][3] = 
        {
            {0,1,2},
            {3,2,1},
            {4,0,6},
            {6,0,2},
            {5,1,4},
            {4,1,0},
            {7,3,1},
            {7,1,5},
            {5,4,7},
            {7,4,6},
            {7,2,3},
            {7,6,2} };

        Vector3f v1 = Vector3f(0,0,0);
        Vector3f v2 = Vector3f(0,0,0);
        Vector3f v3 = Vector3f(0,0,0);
        Vector3f normal = Vector3f(0,0,0);

        Vector2f t1 = Vector2f(0,0);
        Vector2f t2 = Vector2f(0,0);
        Vector2f t3 = Vector2f(0,0);

        setTexture();

        glColor3f(m_gsColor[0],m_gsColor[1],m_gsColor[2]);

        switch(m_gsType) 
        {
            case GS_PLANE:
                {
                    glPushMatrix();

                    float vecLen = 20.f;
                    Vector3f pt0 = m_gsVertices[0] + m_gsVertices[1]*vecLen;
                    Vector3f pt1 = m_gsVertices[0] - m_gsVertices[1]*vecLen;
                    Vector3f pt2 = m_gsVertices[0] + m_gsVertices[2]*vecLen;
                    Vector3f pt3 = m_gsVertices[0] - m_gsVertices[2]*vecLen;

                    glBegin(GL_QUADS);

                    glVertex3f(pt0.x(),pt0.y(),pt0.z());
                    glVertex3f(pt2.x(),pt2.y(),pt2.z());
                    glVertex3f(pt1.x(),pt1.y(),pt1.z());
                    glVertex3f(pt3.x(),pt3.y(),pt3.z());

                    glEnd();

                    glPopMatrix();
                }
                break;
            case GS_BOX:
                {

                    glPushMatrix();

                    glBegin(GL_TRIANGLES);

                    int si=12;
                    for (int i=0;i<si;i++)
                    {

                        v1 = org + rot*m_gsVertices[indices[i][0]];
                        v2 = org + rot*m_gsVertices[indices[i][1]];
                        v3 = org + rot*m_gsVertices[indices[i][2]];

                        t1 = m_gsTexCoords[i*3 + 0];
                        t2 = m_gsTexCoords[i*3 + 1];
                        t3 = m_gsTexCoords[i*3 + 2];

                        // enable reflection on the triangle

                        Vector3f normal = (v3-v1).cross(v2-v1);
                        normal.normalize ();
                        glNormal3f(normal.x(),normal.y(),normal.z());

                        // draws a triangle

                        glTexCoord2f(t1[0],t1[1]);
                        glVertex3f (v1.x(), v1.y(), v1.z());
                        glTexCoord2f(t2[0],t2[1]);
                        glVertex3f (v2.x(), v2.y(), v2.z());
                        glTexCoord2f(t3[0],t3[1]);
                        glVertex3f (v3.x(), v3.y(), v3.z());

                    }
                    glEnd();

                    glPopMatrix();
                }
                break;
            case GS_SPHERE :
                {
                    glPushMatrix();

                    for(int i = 0; i <= (GS_SPHERE_LATS + 1); i++) 
                    {
                        glBegin(GL_QUAD_STRIP);
                        for(int j = 0; j <= (GS_SPHERE_LONGS*2); j+=2) 
                        {

                            v1 = org + rot*m_gsVertices[ i*GS_SPHERE_LONGS*2 + j ];
                            v2 = org + rot*m_gsVertices[ i*GS_SPHERE_LONGS*2 + j + 1 ];
                            t1 = m_gsTexCoords[ i*GS_SPHERE_LONGS*2 + j ];
                            t2 = m_gsTexCoords[ i*GS_SPHERE_LONGS*2 + j + 1  ];

                            glNormal3f(v1.x(),v1.y(),v1.z());
                            glTexCoord2f(t1[0],t1[1]);
                            glVertex3f(v1.x(),v1.y(),v1.z());

                            glNormal3f(v2.x(),v2.y(),v2.z());
                            glTexCoord2f(t2[0],t2[1]);
                            glVertex3f(v2.x(),v2.y(),v2.z());

                        }
                        glEnd();
                    }
                    glPopMatrix();
                }
                break;
            case GS_CONVEX:
                {

                    glPushMatrix();

                    glBegin(GL_TRIANGLES);

                    Vector3f v1;
                    Vector3f v2;
                    Vector3f v3;
                    Vector3f normal;
                    Vector2f t1;
                    Vector2f t2;
                    Vector2f t3;

                    for (unsigned int i = 0; i < m_gsVertices.size()/3; i++)
                    {

                        v1 = org + rot*m_gsVertices[i*3 + 0];
                        v2 = org + rot*m_gsVertices[i*3 + 1];
                        v3 = org + rot*m_gsVertices[i*3 + 2];
                        normal = (v3-v1).cross(v2-v1);
                        normal.normalize ();
                        t1 = m_gsTexCoords[i*3 + 0];
                        t2 = m_gsTexCoords[i*3 + 1];
                        t3 = m_gsTexCoords[i*3 + 2];

                        glNormal3f(normal.x(),normal.y(),normal.z());
                        glTexCoord2f(t1[0],t1[1]);
                        glVertex3f (v1.x(), v1.y(), v1.z());
                        glTexCoord2f(t2[0],t2[1]);
                        glVertex3f (v2.x(), v2.y(), v2.z());
                        glTexCoord2f(t3[0],t3[1]);
                        glVertex3f (v3.x(), v3.y(), v3.z());

                    }
                    glEnd();

                    glPopMatrix();

                }
            default:
                break;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::drawSoft() 
    {

        Vector3f v1 = Vector3f(0,0,0);
        Vector3f v2 = Vector3f(0,0,0);
        Vector3f v3 = Vector3f(0,0,0);
        Vector3f n1 = Vector3f(0,0,0);
        Vector2f t1;
        Vector2f t2;
        Vector2f t3;

        setTexture();

        glColor3f(m_gsColor[0],m_gsColor[1],m_gsColor[2]);

        glPushMatrix();

        glBegin(GL_TRIANGLES);

        bool texture = m_gsTexCoords.size()!=0;

        for(unsigned int n=0; n<m_gsVertices.size()/3; n++) 
        {

            v1 = m_gsVertices[n*3 + 0];
            v2 = m_gsVertices[n*3 + 1];
            v3 = m_gsVertices[n*3 + 2];
            n1 = m_gsNormals[n];
            if(texture) {
                t1 = m_gsTexCoords[n*3 + 0];
                t2 = m_gsTexCoords[n*3 + 1];
                t3 = m_gsTexCoords[n*3 + 2];
            }
            // enable reflection on the triangle

            glNormal3f(n1.x(),n1.y(),n1.z());

            // draws a triangle

            if(texture) glTexCoord2f(t1[0],t1[1]);
            glVertex3f (v1.x(), v1.y(), v1.z());
            if(texture) glTexCoord2f(t2[0],t2[1]);
            glVertex3f (v2.x(), v2.y(), v2.z());
            if(texture) glTexCoord2f(t3[0],t3[1]);
            glVertex3f (v3.x(), v3.y(), v3.z());

        }

        glEnd();

        glPopMatrix();

    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::setTexture() 
    {

        glBindTexture( GL_TEXTURE_2D, m_gsTexture );

    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::initTexture() 
    {

        // texture init
        glGenTextures( 1, &m_gsTexture );
        glBindTexture( GL_TEXTURE_2D, m_gsTexture );

        if(m_gsTextureImage)
            gluBuild2DMipmaps(
                    GL_TEXTURE_2D,3,m_gsTextureWidth,m_gsTextureHeight,
                    GL_RGB,GL_UNSIGNED_BYTE,
                    m_gsTextureImage);

        glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );


        glBindTexture( GL_TEXTURE_2D, 0 );

    }


    ////////////////////////////////////////////////////////////////////////////////
    // GETS ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    int CENSGraphicsShape::getType()
    {
        return m_gsType;
    }

    ////////////////////////////////////////////////////////////////////////////////

    const Vector3f &CENSGraphicsShape::getColor()
    {
        return m_gsColor;
    }


    ////////////////////////////////////////////////////////////////////////////////

    Vertices& CENSGraphicsShape::getVertices()
    {
        return m_gsVertices;
    }

    ////////////////////////////////////////////////////////////////////////////////

    Vector3f& CENSGraphicsShape::getVertex(int vertex_index)
    {
        return m_gsVertices[vertex_index];
    }

    ////////////////////////////////////////////////////////////////////////////////

    Vertices& CENSGraphicsShape::getNormals()
    {
        return m_gsNormals;
    }

    ////////////////////////////////////////////////////////////////////////////////

    Vector3f& CENSGraphicsShape::getNormal(int vertex_index)
    {
        return m_gsNormals[vertex_index];
    }

    ////////////////////////////////////////////////////////////////////////////////

    TexCoords& CENSGraphicsShape::getTexCoords()
    {
        return m_gsTexCoords;
    }

    ////////////////////////////////////////////////////////////////////////////////

    Vector2f& CENSGraphicsShape::getTexCoord(int texcoord_index)
    {
        return m_gsTexCoords[texcoord_index];
    }

    ////////////////////////////////////////////////////////////////////////////////
    // SETS ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::setType(int type)
    {
        m_gsType = type;
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::setColor(const Vector3f &color)
    {
        m_gsColor = color;
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::setVertices(const Vertices &vertices)
    {
        m_gsVertices = vertices;
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::addVertex(const Vector3f &vertex)
    {
        m_gsVertices.push_back(vertex);
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::setNormals(const Vertices &vertices)
    {
        m_gsNormals = vertices;
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::addNormal(const Vector3f &vertex)
    {
        m_gsNormals.push_back(vertex);
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::setTexCoords(const TexCoords &texCoords)
    {
        m_gsTexCoords = texCoords;
    }

    ////////////////////////////////////////////////////////////////////////////////

    void CENSGraphicsShape::addTexCoord(const Vector2f &texCoord)
    {
        m_gsTexCoords.push_back(texCoord);
    }

}
