// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2013 Francesco Mannella
//
// cens_engine.h
// Copyright (c) 2013 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CENS_SERIALIZED_ENGINE_H
#define CENS_SERIALIZED_ENGINE_H

#include "cens_engine.h"
#include "cens_parameter_manager.h"
#include <BulletWorldImporter/btBulletWorldImporter.h>
#include <vector>
#include <map>
#include <string>
#include <sstream>


namespace cens 
{

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    // Inherited by btHashMap to get the array of keys
    template <class Key, class Value>
        class CENSHashMap : public btHashMap<Key, Value>
    {
        public:
            const Key &getKeyAtIndex(int index)
            {
                return (btHashMap<Key, Value>::m_keyArray)[index]; 
            }
    };

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
 
    // Inherited from btBulletWorldImporter to get 
    // the protected maps of rigid bodies and constraints
    // when loading a bullet file
    class CENSImporter : public btBulletWorldImporter
    {
        public:

            CENSImporter(btDynamicsWorld* world=0)
                : btBulletWorldImporter(world) 
            { 
            };

            virtual ~CENSImporter()
            {
            };

            /**
             * @return a vector of names of rigid bodies
             */
            btAlignedObjectArray< char * > &getAllocatedNames()
            {
                return m_allocatedNames;
            }


            /**
             * @return a map of rigid bodies to their names
             */
            btHashMap<btHashString,btRigidBody*> &getBodyMap()
            {
                return m_nameBodyMap;
            }

            /**
             * @return a map of constraints to their names
             */
            btHashMap<btHashString,btTypedConstraint*> &getConstraintMap()
            {
                return m_nameConstraintMap;
            }
    };

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Data structure to import and maintain
     * a serialized hinge
     */
    struct CENSHingeData
    {
        /** The pointer to an hinge constraint */
         CENSHingeConstraint *hinge;
        /** Type of motor control (CENS_DEFAULT_CONTROL/CENS_PD_CONTROL) */
        std::string motor_type;
        /** Motor control is enabled on this hinge */
        std::string  enabled;
        /** Maximum amplitude of motor impulse */
        double max_motor_impulse;
        /** Proportional amplitude (PD control) */
        double kp;
        /** Derivative amplitude (PD control) */
        double kd;
        /** Integration step of the motor controller */
        double motor_dt;
        /** Target position */
        double current_angle;
        /** lover rotation limit */
        double lower_limit;
        /** upper rotation limit */
        double upper_limit;
        /** collision */
        std::string enable_collision;

        CENSHingeData():  
            hinge(0),
            motor_type(CENS_DEFAULT_CONTROL),
            enabled("TRUE"),
            max_motor_impulse(50),
            kp(.99),
            kd(.01),
            motor_dt(0.01),
            current_angle(0),
            lower_limit(-M_PI),
            upper_limit(M_PI),
            enable_collision("FALSE")
        {
        }

        ~CENSHingeData(){}
    };

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Data structure to import and maintain
     * a group of serialized hinges
     */
    struct CENSSerializedRobot : public CENSParameterManager 
    {
        typedef std::map<std::string, CENSHingeData> Hinges;
        typedef std::map<std::string, btTypedConstraint *> GenericConstraints;
        typedef std::map<std::string, btRigidBody *> Bodies;
        typedef std::map<std::string, btTransform> Transforms;
        
        /** Name of the group (corresponds to a file 
         * in the params directory) */
        std::string robotName;
        
        /** Map of hinges to their names */
        Hinges hinges;

        /** Map of non-hinge constraints to their names */
        GenericConstraints constraints;
 
        /** Map of hinges to their names */
        Bodies bodies;

        /** Map of transforms to body names */
        Transforms transforms;
 
        /** A pointer to the engine */
        CENSEngine *engine;


        /**
         * @param robotName the name of the gorup of hinges
         * @param en the engine that uses it
         */
        CENSSerializedRobot(const std::string robotName, CENSEngine *en):
            CENSParameterManager(robotName),engine(en)
        {

        }
        
        ~CENSSerializedRobot()
        {
        }
 
        /** Adds an hinge to the group 
         * @param hingeName the name of the hinge
         * @param _hinge the hinge struct
         */
        void addHinge( std::string hingeName, CENSHingeConstraint *_hinge );

        /** Adds a non-hinge constraint to the group 
         * @param constraintName the name of the constraint
         * @param _constraint pointer to the constraint
         */
        void addGenericConstraint( std::string constraintName, 
                btTypedConstraint *_constraint );

        /** Adds a body to the group 
         * @param bodyName the name of the body
         * @param _body pointer to the body
         */
        void addBody( std::string bodyName, btRigidBody *_body );
        
        /** Adds a transform to the group 
         * @param transformName the name of the body 
         * @param _transform position and rotation of the body
         */
        void addTransform( std::string transformName, btTransform _transform );
        
        /** initialize hinges starting from data in each struct */
        bool init();

        // disable all bodies and hinges in the robot 
        void disableBodies();

        // enable all bodies and hinges in the robot 
        void enableBodies();
 
        /** update hinges' status */
        void update();

        /** 
         * Move a hinge by changing its status (target_angle, inpulse)
         * @param hingeName the name of the hinge to move
         * @param target_angle the new angle to be reached
         * @param impulse the maximum impulse that can be acquired 
         *          while changing angle. This changes hinge.max_motor_impulse
         *          in case of CENS_DEFAULT_CONTROL or hinge.kp in case of
         *          CENS_PD_CONTROL
         * @param prop the proportion between hinge.kp and hinge.kd (only
         *          for CENS_PD_CONTROL).
         */
        void move(const std::string hingeName, double target_angle, 
                double impulse = 0, double prop = 0.001);
    };

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    
    /** maps of groups of bodies */ 
    typedef std::map<std::string, btRigidBody *> Bodies;
    /** maps of groups of bodies */ 
    typedef std::map<std::string, btTypedConstraint *> Constraints;
    /** maps of groups of hinges */ 
    typedef std::map<const std::string, CENSSerializedRobot *> Robots;
    /** maps of groups of sensors */ 
    typedef std::map<const std::string, CENSTouchSensor *> Sensors;
    /** maps of cameras */ 
    typedef std::map<const std::string, int > Cameras;

    class CENSSerializedEngine : public CENSEngine
    {
        public:


            virtual void step(int timestep);

            /**
             * Load rigid bodies and constraints 
             * from a bullet file
             */
            virtual CENSSerializedRobot *loadBulletFile(
                    const std::string &robotName, 
                    const std::string &file);

            /**
             * Add a touch sensor (overrides CENSPhysics method).
             * @param body the body to become a touch sensor
             * @param name the name of the touch sensor
             * @return the pointer to the sensor
             */
            virtual CENSTouchSensor *addTouchSensor(btRigidBody *body, 
                    std::string name)
            {
                CENSTouchSensor *sensor = CENSPhysics::addTouchSensor(body);
                m_eSensors[name] = sensor;

                return sensor;
            }
           
            virtual int addCamera(
                    std::string cameraName,         /**< name of the camera */
                    btRigidBody *body,              /**< The attached body */
                    btTransform local_transform,    /**< transform of the camera */
                    std::string screenTitle,        /**< target view point */     
                    int screenWidth,                /**< width of screen view */
                    int screenHeight,               /**< height of screen view */
                    int screenXGap,                 /**< horizontal screen position of  view */
                    int screenYGap,                 /**< vertical screen position of view */
                    btVector3 up,                   /**< Up vector of the camera */          
                    float target,                   /**< target view point */
                    float foV=70.0,                 /**< field of view */
                    float near=1.0,                 /**< near plane */
                    float far=10000 )               /**< far plane */
            {
                
                int index = attachCamera( body, local_transform, screenTitle,     
                        screenWidth, screenHeight, screenXGap,    
                        screenYGap, up, target, foV, near,far );

                m_eCameras[cameraName] = index;

                return index;
            }

        protected:

            /** contains all hinges' groups owned by an Engine */
            Robots m_eRobots;

            /** 
             * contains all sensors owned by an Engine labeled with 
             * the name of their rigid body
             */
            Sensors m_eSensors;
            
            /** 
             * contains all camera indices labeled with 
             * the name of their rigid body
             */
            Cameras m_eCameras;

            /** 
             * contains all bodies owned by an Engine labeled with 
             * a name 
             */
            Bodies m_eBodies;

            /** 
             * contains all constraints owned by an Engine labeled with 
             * a name 
             */
            Constraints m_eConstraints;
    };

}

#endif // CENS_SERIALIZED_ENGINE_H

