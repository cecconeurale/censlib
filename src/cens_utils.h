// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_utils.h
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CENS_UTILS_H
#define CENS_UTILS_H
#include <vector>
#include <string>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>

namespace cens {

  ///////////////////////////////////////////////////////////////////////////////////////////////
  //// CENS_UTILS ///////////////////////////////////////////////////////////////////////////////

  typedef std::vector<std::string> strings;

  //// MATH /////////////////////////////////////////////////////////////////////////////////////

  inline float rad2degree(float grad){
    return grad*(M_PI/180.0);
  }

  inline float degree2rad(float degree){
    return degree*(M_PI/180.0);
  }


  //// STRING PARAMETERS ////////////////////////////////////////////////////////////////////////

  float scalarFromString(std::string str);

  std::string str_replace(std::string str, 
      const std::string &searchString, 
      const std::string &replaceString );
 
  std::vector<std::string> str_split(std::string str,
      const std::string &sep);

  bool str_match(const std::string &str,
      const std::string &comp);

}

#endif
