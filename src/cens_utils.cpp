// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_utils.cpp
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#include "cens_utils.h"
#include <cassert>

namespace cens {

  ///////////////////////////////////////////////////////////////////////////////////////////////
  //// INIT PARAMETERS //////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  float scalarFromString(std::string str) {
    return std::strtod( str.c_str(), 0 );
  }

  
  std::string str_replace(std::string str, 
      const std::string &searchString, 
      const std::string &replaceString )
  {

    assert ( searchString != replaceString );

    std::string::size_type pos = 0;
    while ( (pos = str.find(searchString, pos)) != std::string::npos ) {
      str.replace( pos, searchString.size(), replaceString );
      pos++;
    }

    return str;
  }




  std::vector<std::string> str_split(std::string str,
      const std::string &sep) {

    std::vector<std::string> res;
    std::string mod_sep(sep);
    std::string tmp;

    mod_sep=str_replace(mod_sep," ","_SPACE_");
    str=str_replace(str," ","_SPACE_");

    str=str_replace(str,mod_sep," ");
    std::stringstream stream(str);
    while(!stream.eof()) {
      stream >> tmp;
      res.push_back(str_replace(tmp,"_SPACE_"," "));
    }


    return res;
  }


  bool str_match(const std::string &str,
      const std::string &comp) {

    size_t found = str.find(comp);
    return (found != std::string::npos)?true:false;

  }

}
