// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_engine.h
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CENS_ENGINE_H
#define CENS_ENGINE_H

#include <iostream>

#include "cens_physics.h"
#include "cens_graphics.h"
#include "cens_types.h"



/**
 * @brief Computational Embodied Neuroscience Simulator library
 */
namespace cens 
{

    ///////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////
    // Eigen - bt convertions ///////////////////////
    /////////////////////////////////////////////////

    /** Get an eigen Vector3f from a bullet btVector3 */
    Eigen::Vector3f btVec2eigen(const btVector3& v);

    /** Get a bullet btVector3 from an eigen Vector3f */
    btVector3 eigen2btVec(const Eigen::Vector3f& v);

    /** Get a bullet btMatrix3x3 from an eigen Matrix3f */
    btMatrix3x3 eigen2btMat(const Eigen::Matrix3f& v);

    /** Get an eigen Matrix3f from a bullet btMatrix3x3 */
    Eigen::Matrix3f btMat2eigen(const btMatrix3x3& v);


    /** maps of graphics shapes to bodies */
    typedef std::map< btCollisionObject *, CENSGraphicsShape * > CENSShapes;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief Initializing
     * and manipulating physics objects.
     *
     * Graphic rendering is automatic,
     * based on information stored in
     * physics.
     *
     * Inherits CENSGraphics and CENSPhysics.
     * 
     * The initObjects() and the step()
     * methods can be overloaded in a
     * derived class.
     * 
     * In the initObjects() method a model
     * of the world can be built by
     * initializing all bodies and joints.
     * Pointers bodies and joints to be
     * manipulated at run time should be
     * declared in the header as members of
     * the class. 
     *
     * In the step() method run-time
     * you can put instructions about
     * moving and resetting bodies and
     * joints as well as applying forces and 
     * torques, etc.  Remember to call the
     * CENSEngine::step() method at the end of
     * the overloaded step() method
     * @verbatim
     void DerivedFromCENSEngine::step( int value) { 
     ...  
     ...
     CENSEngine::step(value); 
     } 
     @endverbatim
     *
     */


    class CENSEngine: public CENSPhysics, public CENSGraphics  
    {
        public:

            CENSEngine(); 

            virtual ~CENSEngine();

            /** Initialize cens_graphics and cens_physics.   */
            virtual void init( 
                    int argc,       /**< arguments' number (for glutMainLoop). */
                    char **argv     /**< arguments' list (for glutMainLoop). */  );

            virtual void initObjects() {};

            virtual void display();

            virtual void step(int value );

            /** Start the simulation. */
            virtual void run();
           
            /** run a single step of simulation. */
            virtual void stepRun();

            /** Create a rigid body and link
             * it to the m_phDynamicsWorld. 
             * @param     mass            weight of the body.
             *                            Zero mass equals to a body
             *                            having a fixed position in
             *                            space.
             * @param     startTransform  initial rotation and 
             *                            translation.
             * @param     shape           shape of the body.
             * @param     color           color of the body.
             * @param     texCoords       UV cordinates for
             *                            texture.
             * @param     pixmap          image to use for
             *                            texturing the surface of the
             *                            body. 
             */
            btRigidBody* localCreateRigidBody(
                    float mass, 
                    const btTransform& startTransform,
                    btCollisionShape* shape, 
                    const btVector3 &color = eigen2btVec(CENS_NULL_COLOR),
                    const TexCoords &texCoords = CENS_NULL_TEXCOORDS,
                    CENSPixelMap &pixmap = CENS_NULL_PIXMAP  );

            /** Link a rigid body
             * it to the m_phDynamicsWorld. 
             * @param     body            pointer to a btRigidBody
             * @param     color           color of the body.
             * @param     texCoords       UV cordinates for
             *                            texture.
             * @param     pixmap          image to use for
             *                            texturing the surface of the
             *                            body. 
             */
            btRigidBody* localImportRigidBody(
                    btRigidBody* body, 
                    const btVector3 &color = eigen2btVec(CENS_NULL_COLOR),
                    const TexCoords &texCoords = CENS_NULL_TEXCOORDS,
                    CENSPixelMap &pixmap = CENS_NULL_PIXMAP  );

            /** Create a soft body and link
             * it to the m_phDynamicsWorld. 
             * @param     mass            weight of the body.
             *                            Zero mass equals to a body
             *                            having a fixed position in
             *                            space.
             * @param     shape           shape of the body.
             * @param     color           color of the body.
             * @param     texCoords       UV cordinates for
             *                            texture.
             * @param     pixmap          image to use for
             *                            texturing the surface of the
             *                            body. 
             */
            btSoftBody* localCreateSoftBody(
                    float mass, 
                    btCollisionShape* shape,
                    const btVector3 &color = eigen2btVec(CENS_NULL_COLOR),
                    const TexCoords &texCoords = CENS_NULL_TEXCOORDS,
                    CENSPixelMap &pixmap = CENS_NULL_PIXMAP  );

            /** Link a rigid body
             * it to the m_phDynamicsWorld. 
             * @param     body            pointer to a btRigidBody
             * @param     color           color of the body.
             * @param     texCoords       UV cordinates for
             *                            texture.
             * @param     pixmap          image to use for
             *                            texturing the surface of the
             *                            body. 
             */
            btSoftBody* localLinkSoftBody(
                    btSoftBody* body,
                    const btVector3 &color = eigen2btVec(CENS_NULL_COLOR),
                    const TexCoords &texCoords = CENS_NULL_TEXCOORDS,
                    CENSPixelMap &pixmap = CENS_NULL_PIXMAP  );


            /** Attach a camera to a body */
            virtual int attachCamera( 
                    btRigidBody *body,              /**< The attached body */
                    btTransform local_transform,    /**< transform of the camera */
                    std::string screenTitle,        /**< target view point */     
                    int screenWidth,                /**< width of screen view */
                    int screenHeight,               /**< height of screen view */
                    int screenXGap,                 /**< horizontal screen position of  view */
                    int screenYGap,                 /**< vertical screen position of view */
                    btVector3 up,                   /**< Up vector of the camera */          
                    float target,                   /**< target view point */
                    float foV=70.0,                 /**< field of view */
                    float near=1.0,                 /**< near plane */
                    float far=10000                 /**< far plane */  
                    );
        protected:

            /** 
             * @brief Parameters for the body-camera link
             * 
             * A camera can be attached to a body, this structure
             * allows to store the information for this kind of link
             */
            struct CENSBodyCameraData 
            {

                /** Index of the camera in the camera store */
                int m_bcIdx;
                /** the linked body - must be a rigid body */
                btRigidBody *m_bcBody;
                /** the local transform */
                btTransform m_bcLocal_transform;
                /** the local origin */
                btVector3 m_bcOrigin;
                /** the local target */
                btVector3 m_bcTarget;
                /** the local up */
                btVector3 m_bcUp;
            };

            /** shapes mapped to bodies */
            CENSShapes m_eShapes;

            /** Store of the attached cameras */
            std::vector<CENSBodyCameraData> m_eAttachedCameras;

            /** synchronize the camera position with the body position */
            virtual void syncAttachedCamera(
                    int index /**< Index of the camera */
                    );

    };

}

#endif // CENS_ENGINE_H

