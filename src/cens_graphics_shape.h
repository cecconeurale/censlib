// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_graphics_shape.h
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CENS_GRAPHICS_SHAPE_H
#define CENS_GRAPHICS_SHAPE_H

#include <vector>

// linear algebra
#include <Eigen/Core>
#include <Eigen/Geometry>


using namespace Eigen;

// OpenGL
#include <GL/glut.h>
#include <GL/freeglut.h>

#include "cens_pixelmap.h"
#include "cens_types.h"


namespace cens {

    /**
     * @brief Graphic processing of an object. 
     * 
     * It manages rendering of shape, texture and color 
     * of an object. 
     */
    class CENSGraphicsShape 
    {

        public:

            CENSGraphicsShape() 
            { 
                m_gsType = GS_NULL;
                m_gsColor = Vector3f(0,0,0);
                m_gsTexture = 0;
                m_gsTextureImage = 0;
                m_gsTextureWidth = 0;
                m_gsTextureHeight = 0;
            };

            CENSGraphicsShape(const CENSGraphicsShape &copy) 
            {
                m_gsType = copy.m_gsType;
                m_gsColor = copy.m_gsColor;
                m_gsVertices = copy.m_gsVertices;
                m_gsTexture = copy.m_gsTexture;
                m_gsTextureWidth = copy.m_gsTextureWidth;
                m_gsTextureHeight = copy.m_gsTextureHeight;
                copyimage(copy.m_gsTextureImage);
            }

            ~CENSGraphicsShape();

            /** 
             * Builds a plane.
             *
             * @param orig up versor of the plane.
             * @param v1 first orthogonal versor.
             * @param v2 second orthogonal versor.
             * @param color color of the plane.
             * @param pixmap picture for the texture.
             */
            void buildAsPlane(
                    const Vector3f &orig, 
                    const Vector3f &v1, 
                    const Vector3f &v2, 
                    const Vector3f &color = CENS_NULL_COLOR, 
                    CENSPixelMap pixmap = CENS_NULL_PIXMAP );
            /** 
             * Builds a box.
             *
             * @param halfExtents halves of box dimentions.
             * @param color color of the box.
             * @param pixmap picture for the texture.
             */
            void buildAsBox(
                    const Vector3f &halfExtents, 
                    const Vector3f &color = CENS_NULL_COLOR, 
                    CENSPixelMap pixmap = CENS_NULL_PIXMAP );
            /** 
             * Builds a sphere.
             *
             * @param radius radius of the sphere.
             * @param color color of the sphere.
             * @param pixmap picture for the texture.
             */
            void buildAsSphere(
                    float radius, 
                    const Vector3f &color = CENS_NULL_COLOR, 
                    CENSPixelMap pixmap = CENS_NULL_PIXMAP);
            /** 
             * Builds a convex hull.
             *
             * @param   vertices    a vector containing all the vertices 
             *                      of the hull.
             * @param   idx         a vector containing a sequences of indexes 
             *                      to the vertices vector. Vertices are indexed 
             *                      in order to form the triangles of the mesh.
             * @param   nvtxs       number of vertices.
             * @param   nidxs       number of indices.
             * @param   ntrns       number of triangles.
             * @param   texCoords   UV cordinates for
             *                      texture.
             * @param   color       color of the hull.
             * @param   pixmap      picture for the texture.
             */
            void buildAsConvex(
                    const Vertices &vertices, 
                    const unsigned int *idx, 
                    int nvtxs, 
                    int nidxs, 
                    int ntrns, 
                    const TexCoords &texCoords = CENS_NULL_TEXCOORDS, 
                    const Vector3f &color = CENS_NULL_COLOR, 
                    CENSPixelMap pixmap = CENS_NULL_PIXMAP  );
            /** 
             * Builds a soft body
             *
             * @param   numTriangles    number of triangles.
             * @param   texCoords       UV cordinates for
             *                          texture.     
             * @param   color           color of the soft body.
             * @param   pixmap          picture for the texture.
             */
            void buildAsSoft(
                    int numTriangles,
                    const TexCoords &texCoords = CENS_NULL_TEXCOORDS, 
                    const Vector3f &color = CENS_NULL_COLOR, 
                    CENSPixelMap pixmap = CENS_NULL_PIXMAP );
            /** 
             * Rendering of rigid objects.
             * 
             * @param org position of the center of mass of the object.
             * @param rot rotation from inittial position of the object.
             */
            void draw(
                    const Vector3f &org, 
                    const Matrix3f &rot);
            /** 
             * Rendering of soft bodies.
             */
            void drawSoft();
            /**
             *  Copy the texture picture.
             *
             *  @param source array of unsigned bytes 
             *                where the image is stored.
             */
            void copyimage(const GLubyte *source);
            /**
             * Set texturing for the object.
             */
            void setTexture();
            /**
             *  Initialize texturing for the object.
             */
            void initTexture();
            /**
             * @return the object type: can be one of
             GS_NULL, GS_PLANE, GS_BOX, 
             GS_SPHERE, GS_CONVEX or GS_SOFT;
             */
            int getType();
            /**
             * @return a RGB vector with 0.0 to 1.0 elements
             */
            const Vector3f &getColor();    
            /**
             * @return a vector of XYZ positions
             */
            Vertices & getVertices();
            /**
             * @param vertex_index the index of a vertex
             * @return an XYZ position
             */
            Vector3f &getVertex(int vertex_index);
            /**
             * @return a vector of normals
             */
            Vertices & getNormals();
            /**
             * @param normal_index index of a normal
             * @return a normal
             */
            Vector3f &getNormal(int normal_index);
            /**
             * Get UV texture coordinates
             * @return a vector of UV coordinates
             */
            TexCoords & getTexCoords();
            /**
             * An UV texture coordinate
             * @param texCoord_index index of the UV coordinate
             * @return an UV coordinate
             */
            Vector2f &getTexCoord(int texCoord_index);
            /**
             * The object type
             *
             * @param type  one of GS_NULL, GS_PLANE, GS_BOX, 
             GS_SPHERE, GS_CONVEX or GS_SOFT;
             */
            void setType(int type);
            /**
             * @param color a RGB vector with 0.0 to 1.0 elements
             */    
            void setColor(const Vector3f &color);
            /**
             * @param vertices a vector of XYZ positions
             */    
            void setVertices(const Vertices &vertices);
            /**
             * @param vertex an XYZ position
             */    
            void addVertex(const Vector3f &vertex);
            /**
             * @param normals a vector of normals
             */    
            void setNormals(const Vertices &normals);
            void addNormal(const Vector3f &normal);
            /**
             * Set UV texture coordinates
             * @param texCoords a vector of UV coordinates
             */
            void setTexCoords(const TexCoords &texCoords);
            /**
             * Add an UV texture coordinate
             * @param texCoord an UV coordinate
             */
            void addTexCoord(const Vector2f &texCoord);

        private:

            int m_gsType;
            Vector3f m_gsColor;
            Vertices m_gsVertices;
            Vertices m_gsNormals;
            TexCoords m_gsTexCoords;
            GLuint m_gsTexture;
            GLubyte *m_gsTextureImage;
            int m_gsTextureWidth;
            int m_gsTextureHeight;

    };

}


#endif //CENS_GRAPHICS_SHAPE_H
