#ifndef CENS_PARAMETER_MANAGER_H
#define CENS_PARAMETER_MANAGER_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

// linear algebra
#include <Eigen/Core>
#include <Eigen/Geometry>
using namespace Eigen;
        

class CENSParameterDirException: public std::runtime_error
{

    public:

        CENSParameterDirException(std::string const& msg = "parameter directory (\"./params\") not present!"):
            std::runtime_error(msg)
            {}
};

class CENSParameterException: public std::runtime_error
{
 
    public:
 
        CENSParameterException(std::string const& msg = "parameter file not present ... initialized!"):
            std::runtime_error(msg)
            {}
};

/**
 * @brief Manager of parameters' initializarion
 */
class CENSParameterManager
{

    public:

        typedef std::map<std::string,std::string *> strvalues;
        typedef strvalues::iterator striter;      
        typedef std::map<std::string,Vector4f *> v4values;
        typedef v4values::iterator v4iter;      
        typedef std::map<std::string,Vector3f *> v3values;
        typedef v3values::iterator v3iter;      
        typedef std::map<std::string,double *> dvalues;
        typedef dvalues::iterator diter;      
        typedef std::map<std::string,float *> fvalues;
        typedef fvalues::iterator fiter;      
        typedef std::map<std::string,int *> ivalues;
        typedef ivalues::iterator iiter;      
        typedef std::map<int, std::string> labels;
        typedef labels::iterator liter;      
  
        /** Constructor */
        CENSParameterManager(
                std::string _filename, 
                const char *sep="-------",
                const char *vsep=";",
                const char *filedir="parameters/") :
            filename(std::string(filedir).append(_filename)),
            separator(sep),
            vseparator(vsep),
            added(0)
        {
            if (not boost::filesystem::exists(filedir))
                if(not boost::filesystem::create_directory(filedir))
                    throw CENSParameterDirException();
        }
  
        CENSParameterManager(const CENSParameterManager &copy)  
        {
            separator = copy.separator;
            separator = copy.vseparator;
            filename = copy.filename;
            strparlist = copy.strparlist;
            v4parlist = copy.v4parlist;
            v3parlist = copy.v3parlist;
            dparlist = copy.dparlist;
            fparlist = copy.fparlist;
            iparlist = copy.iparlist;
            order = copy.order;
            added = copy.added;
        }
  
  
        /** Add a parameter to the list */
        void addParameter(std::string parname, std::string &par)
        {
            strparlist[parname] = &par;
            order[added++]=parname;
        }
   
        /** Add a parameter to the list */
        void addParameter(std::string parname, Vector4f &par)
        {
            v4parlist[parname] = &par;
            order[added++]=parname;
        }
   
        /** Add a parameter to the list */
        void addParameter(std::string parname, Vector3f &par)
        {
            v3parlist[parname] = &par;
            order[added++]=parname;
        }
   
        /** Add a parameter to the list */
        void addParameter(std::string parname, double &par)
        {
            dparlist[parname] = &par;
            order[added++]=parname;
        }
   
        /** Add a parameter to the list */
        void addParameter(std::string parname, float &par)
        {
            fparlist[parname] = &par;
            order[added++]=parname;
        }
   
        /** Add a parameter to the list */
        void addParameter(std::string parname, int &par)
        {
            iparlist[parname] = &par;
            order[added++]=parname;
        }
  
        /** Get parameters from file */
        bool loadParameters() {
       
            // load parameter file
            std::ifstream loadstr(
                    filename.c_str()); 
            if(!loadstr.is_open()) 
            {
                return false;
            }
        
            // Load all strings from the file of parameters 
            std::vector<std::string> paramfile_strings;
            std::string line("");
            while( std::getline( loadstr, line ) )
            {
                paramfile_strings.push_back( line );
            }
            paramfile_strings.push_back( line );
        
        
            ////////////////////////////////////////////////////////////
            //// scan strparamlist for parameters to manipulate
            for( 
                    striter iter = strparlist.begin(); 
                    iter != strparlist.end(); 
                    ++iter ) 
            {
       
                // built regex pattern representing the parameter label
                std::string pattern = 
                    std::string("\\s*")
                    .append(separator)
                    .append("\\s*\\<")
                    .append(iter->first )
                    .append("\\>\\s*$");
                
 
                boost::regex re(  pattern );   
         
                // extract value from param line
                for( 
                        unsigned i=0; 
                        i<paramfile_strings.size(); 
                        i++ ) 
                {
                    if ( boost::regex_search( 
                                paramfile_strings[i], 
                                re ) ) 
                    {  
                    
                        std::string rep =  
                            boost::regex_replace(
                                    paramfile_strings[i] , 
                                    re, "" );

                        *(iter->second) = rep;

                    }
                }
            }
        
        
            ////////////////////////////////////////////////////////////
            //// scan v4paramlist for parameters to manipulate
            for( 
                    v4iter iter = v4parlist.begin(); 
                    iter != v4parlist.end(); 
                    ++iter ) 
            {
       
                // built regex pattern representing the parameter label
                std::string pattern = 
                    std::string("\\s*")
                    .append(separator)
                    .append("\\s*\\<")
                    .append(iter->first )
                    .append("\\>\\s*$");
                
 
                boost::regex re(  pattern );   
         
                // extract value from param line
                for( 
                        unsigned i=0; 
                        i<paramfile_strings.size(); 
                        i++ ) 
                {
                    if ( boost::regex_search( 
                                paramfile_strings[i], 
                                re ) ) 
                    {  
                    
                        Vector4f &v4 = *(iter->second);
 
                        std::string rep =  
                            boost::regex_replace(
                                    paramfile_strings[i] , 
                                    re, "" );

                        boost::sregex_token_iterator i(
                                rep.begin(), 
                                rep.end(), 
                                boost::regex(vseparator), -1);

                        std::stringstream(*i++) >> v4[0];
                        std::stringstream(*i++) >> v4[1];
                        std::stringstream(*i++) >> v4[2];
                        std::stringstream(*i++) >> v4[3];

                    }
                }
            }
        
        
            ////////////////////////////////////////////////////////////
            //// scan v3paramlist for parameters to manipulate
            for( 
                    v3iter iter = v3parlist.begin(); 
                    iter != v3parlist.end(); 
                    ++iter ) 
            {
       
                // built regex pattern representing the parameter label
                std::string pattern = 
                    std::string("\\s*")
                    .append(separator)
                    .append("\\s*\\<")
                    .append(iter->first )
                    .append("\\>\\s*$");
                
 
                boost::regex re(  pattern );   
         
                // extract value from param line
                for( 
                        unsigned i=0; 
                        i<paramfile_strings.size(); 
                        i++ ) 
                {
                    if ( boost::regex_search( 
                                paramfile_strings[i], 
                                re ) ) 
                    {  
                    
                        Vector3f &v3 = *(iter->second);
 
                        std::string rep =  
                            boost::regex_replace(
                                    paramfile_strings[i] , 
                                    re, "" );

                        boost::sregex_token_iterator i(
                                rep.begin(), 
                                rep.end(), 
                                boost::regex(vseparator), -1);

                        std::stringstream(*i++) >> v3[0];
                        std::stringstream(*i++) >> v3[1];
                        std::stringstream(*i++) >> v3[2];

                    }
                }
            }
        
        
            ////////////////////////////////////////////////////////////
            //// scan dparamlist for parameters to manipulate
            for( 
                    diter iter = dparlist.begin(); 
                    iter != dparlist.end(); 
                    ++iter ) 
            {
       
                // built regex pattern representing the parameter label
                std::string pattern = 
                    std::string("\\s*")
                    .append(separator)
                    .append("\\s*\\<")
                    .append(iter->first )
                    .append("\\>\\s*$");
                
 
                boost::regex re(  pattern );   
         
                // extract value from param line
                for( 
                        unsigned i=0; 
                        i<paramfile_strings.size(); 
                        i++ ) 
                {
                    if ( boost::regex_search( 
                                paramfile_strings[i], 
                                re ) ) 
                    {  
                        std::string rep =  
                            boost::regex_replace(
                                    paramfile_strings[i] , 
                                    re, "" );
                        std::stringstream(rep) >> *(iter->second); 
                    }
                }
            }
        
            ////////////////////////////////////////////////////////////
            //// scan fparamlist for parameters to manipulate
            for( 
                    fiter iter = fparlist.begin(); 
                    iter != fparlist.end(); 
                    ++iter ) 
            {
       
                // built regex pattern representing the parameter label
                std::string pattern = 
                    std::string("\\s*")
                    .append(separator)
                    .append("\\s*\\<")
                    .append(iter->first )
                    .append("\\>\\s*$");
                
 
                boost::regex re(  pattern );   
         
                // extract value from param line
                for( 
                        unsigned i=0; 
                        i<paramfile_strings.size(); 
                        i++ ) 
                {
                    if ( boost::regex_search( 
                                paramfile_strings[i], 
                                re ) ) 
                    {  
                        std::string rep =  
                            boost::regex_replace(
                                    paramfile_strings[i] , 
                                    re, "" );
                        std::stringstream(rep) >> *(iter->second); 
                    }
                }
            }
 
            ////////////////////////////////////////////////////////////
            //// scan iparamlist for parameters to manipulate
            for( 
                    iiter iter = iparlist.begin(); 
                    iter != iparlist.end(); 
                    ++iter ) 
            {
       
                // built regex pattern representing the parameter label
                std::string pattern = 
                    std::string("\\s*")
                    .append(separator)
                    .append("\\s*\\<")
                    .append(iter->first )
                    .append("\\>\\s*$");
                boost::regex re(  pattern );  
        

                // extract value from param line
                for( 
                        unsigned i=0; 
                        i<paramfile_strings.size(); 
                        i++ ) 
                {
                    if ( boost::regex_search( 
                                paramfile_strings[i], 
                                re ) ) 
                    {  
                        std::string rep =  
                            boost::regex_replace(
                                    paramfile_strings[i] , 
                                    re, "" );
                        std::stringstream(rep) >> *(iter->second); 
                    }
                }
            }

            return true;
   
        }
  
        /** save parametrs in list */
        void saveParameters()
        {
 
            std::ofstream file(filename.c_str());
            
            for( 
                    liter iter = order.begin(); 
                    iter != order.end(); 
                    ++iter ) 
            {

                std::string name = iter->second;

                if (iparlist.find(name) != iparlist.end())
                        file    << *iparlist[name] 
                                << " " << separator << " " << name << std::endl;
                else if (fparlist.find(name) != fparlist.end())
                        file    << *fparlist[name] 
                                << " " << separator << " " << name << std::endl;
                else if (dparlist.find(name) != dparlist.end())
                        file    << *dparlist[name] 
                                << " " << separator << " " << name << std::endl;
                else if (strparlist.find(name) != strparlist.end())
                        file    << *strparlist[name] 
                                << " " << separator << " " << name << std::endl;
                else if (v3parlist.find(name) != v3parlist.end())
                        file    << (*v3parlist[name])[0] << vseparator 
                                << (*v3parlist[name])[1] << vseparator
                                << (*v3parlist[name])[2] 
                                << " " << separator << " " << name << std::endl;
                else if (v4parlist.find(name) != v4parlist.end())
                        file    << (*v4parlist[name])[0] << vseparator 
                                << (*v4parlist[name])[1] << vseparator
                                << (*v4parlist[name])[2] << vseparator
                                << (*v4parlist[name])[3] 
                                << " " << separator << " " << name << std::endl;
            }
        }

    protected:
  
      
        /** Parameters data-file  */
        std::string filename;
      
        const char *separator;
        const char *vseparator;

        /**
         * List of Vector3f type parameters to manipulate 
         */
        strvalues strparlist;
     
        /**
         * List of Vector3f type parameters to manipulate 
         */
        v4values v4parlist;
     
        /**
         * List of Vector3f type parameters to manipulate 
         */
        v3values v3parlist;
     
        /**
         * List of double type parameters to manipulate 
         */
        dvalues dparlist;
     
        /**
         * List of float type parameters to manipulate 
         */
        fvalues fparlist;
     
        /**
         * List of integer type parameters to manipulate 
         */
        ivalues iparlist;
    
        /**
         * Storage of parameters' order
         */
        labels order;
     
        /**
         * index of last added parameter  
         */
        int added;
};
#endif //CENS_PARAMETER_MANAGER_H
