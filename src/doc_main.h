// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_engine.h
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.


#include "cens_physics.h"
#include "cens_graphics.h"
#include "cens_types.h"


namespace cens 
{

    /** \mainpage <h1> CENSLIB - Computational Embodied Neuroscience Simulation Library</h1>
     * 
     * @authors Francesco Mannella
     *
     * Download page: <a href="http://sourceforge.net/projects/censlib/">http://sourceforge.net/projects/censlib/</a>
     *
     * <div style="float: left;margin:2em 3em;">
     * <img src="pics/motor.png" width="300"/>
     * <a href="http://www.youtube.com/watch?feature=player_embedded&v=QbheU28w2To">Demo showing motor control and touch sensor </a>
     * </div>
     * \section intro Introduction
     * This software allows to build 3D simulated environments and robots
     * using <a href="http://bulletphysics.org/wordpress/"> Bullet physics
     * library</a> for physics simulation and <a
     * href="http://www.opengl.org/"> OpenGL</a> for rendering. The library
     * contains the CENSEngine class that is able to automatize the rendering
     * of objects created through the physics engine. Moreover the
     * CENSSerializedEngine::loadBulletFile method contained in
     * CENSSerializedEngine derived from CENSEngine completely automatize the
     * creation of btRigidBody and btHingeConstraint objects and their usage.
     * Objects and constraints can be designed in a visual mode using <a
     * href="http://www.blender.org"> Blender</a> and then imported into
     * CENSLIB through the method CENSSerializedEngine::loadBulletFile.  By
     * this mean writing bullet code directly can also be avoided.  
     *
     * The project start code is the DemoApplication class present in the
     * Demos/OpenGL folder in the <a
     * href="http://code.google.com/p/bullet/downloads/list"> Bullet
     * physics library </a> package.  The idea of this library was born
     * from the need to have a cleaner code to allow users to bypass all
     * graphics rendering issues and focus on the bullet code.
     *
     * @tableofcontents
     * 
     * \section install Installation
     * 
     * The CENSLIB library is meant to be used on a linux platform.  It depends
     * on bullet library and OpenGL. Other needed libraries are the <a
     * href="http://eigen.tuxfamily.org/dox-2.0/"> Eigen2 </a> linear algebra
     * library, used for data manipulation within the graphics module, the <a
     * href="http://www.imagemagick.org/Magick++/"> Magick++ </a> library used
     * to manipulate screenshots of simulations as pixmaps and <a
     * href="http://www.boost.org/">boost</a> libraries used for regular
     * expressions and file management.
     *
     * On a debian distribution install these packages through this command:
     * 
     \verbatim
     sudo apt-get install libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev \
            libmagick++-dev libeigen2-dev libboost-regex-dev \
            libboost-system-dev libboost-filesystem-dev libxmu-dev libxi-dev
     \endverbatim
     * 
     * Then install bullet 2.82 release. On a terminal:
     *
     \verbatim
     svn checkout http://bullet.googlecode.com/svn/tags/bullet-2.82/ bullet
     cd bullet; mkdir cmake_build; cd cmake_build
     cmake  .. -DBUILD_SHARED_LIBS=ON -DBUILD_EXTRAS=ON -DINSTALL_LIBS=ON -DINSTALL_EXTRA_LIBS=ON
     make -j4
     sudo make install
     \endverbatim
     *
     * Now download the latest CENSLIB version from 
     * <a href="http://sourceforge.net/projects/censlib/">here</a>,
     * untar, build  and install it:
     *
     \verbatim
     tar xzvf censlib-<release-number>.tar.gz
     cd CENSLIB
     \endverbatim
     and you can build it using cmake :
     \verbatim 
     mkdir build; cd build
     cmake ..
     make -j4
     sudo make install
     \endverbatim
   
     or the Autotools: 
     
     \verbatim 
     mkdir build; cd build
     ../configure
     make -j4 
     sudo make install
     \endverbatim

     *
     * \section usage Usage
     *
     * The following command Compiles and links an example.cpp source file:
     \verbatim
     c++ -O2 -o example example.cpp \
        -I/usr/local/include/CENS \
        -I/usr/local/include/bullet -I/usr/include/eigen2 \
        -I/usr/include/ImageMagick \
        -lCENS-1.1 \
        -lBulletWorldImporter -lBulletFileLoader \
        -lBulletSoftBody -lBulletDynamics \
        -lBulletCollision -lLinearMath \
        -lboost_regex -lboost_system -lboost_filesystem\
        -lGL -lGLU -lglut \
        -lMagick++ 
     \endverbatim
     * 
     * \section basics Getting started 
     *
     * \subsection first Building a simple program
     *
     * In a typical simulation using CENSLIB 
     * a  new class is derived from  CENSEngine:
     *
     \code{.cpp} 
     #include "cens_engine.h"
     using namespace cens;
     
     class MyEngine : public CENSEngine 
     {   
         public:   
             // overloadings
             void initObjects();
             void step( int step_interval);
     };
     \endcode
     * Two methods must be overloaded. One of them is
     * CENSEngine::initObjects where you write 
     * bullet code about the initialization of your objects 
     * and constraints :  
     \code{.cpp}
     void MyEngine::initObjects()
     {
         // YOUR BULLET CODE
         // for instance we build a simple cube:
         
         // a box shape
         btCollisionShape* shape = 
             new btBoxShape( btVector3(1,1,1) ); 

         // a transform centered on the origin
         btTransform transform;
         transform.setIdentity();
 
         // the mass of the object
         double mass = 1.0;

         // let's build the object!
         btRigidBody *body =  
             localCreateRigidBody(
                     mass, transform, shape);

     }
     \endcode
     * Note how the building of a btRigidBody is simplified by 
     * the use of the method CENSEngine::localCreateRigidBody, which
     * automatically build and attach a btRigidBody to the world starting 
     * from a shape, a transform and a mass value.
     *
     * The other method to overload is CENSEngine::step. 
     * You can write here your bullet code for applying forces 
     * at each timestep:
     *
     *
     \code{.cpp}
     void MyEngine::step(int step_interval)
     {
         // YOUR BULLET CODE

         CENSEngine::step(timestep);
     }
     \endcode
     * the call to CENSEngine::step must always 
     * be at the end of your overloaded step method,
     * otherwise the simulation does not start.
     *
     * Once you built your class derived from CENSEngine
     * you only need to define an object of that type
     * and call its CENSEngine::init and CENSEngine::run
     * methods in the main function:
     \code{.cpp}
     int main(int argc, char** argv) 
     {
         MyEngine engine;
         engine.init(argc,argv);
         
         engine.run();
         return 0;
     }
     \endcode
     *
     * 
     * When the program runs the first time two files are searched for in the
     * ./parameters directory, one called "physics_parameters", the other
     * called "graphics_parameters". They contain the values of graphics and
     * physics parameters respectiivelly. Both can bechanged offline before 
     * starting a simulation. if those files do not exist they are
     * created with default values:
     * 
     * graphics_parameters 
     \verbatim
        800             ------- SCREEN_WIDTH      <- pixel width of the window
        600             ------- SCREEN_HEIGHT     <- pixel height of the window
        10              ------- SCREEN_XGAP       <- horizontal position of the window
        10              ------- SCREEN_YGAP       <- vertical position of the window
        10              ------- TIMESTEP          <- time interval of a single timestep (milliseconds)
        Demo            ------- SCREEN_TITLE      <- title of the window
        70              ------- FIELD_OF_VIEW     <- the angle (degrees) defining the width of the FoV
        1.2             ------- RATIO             <- ratio between horizontal/vertical FoV angle
        1               ------- NEAR              <- near plane of view
        10000           ------- FAR               <- far plane of view
        0.15            ------- ANGLE             <- from where the camera points to the target
        15              ------- DISTANCE          <- distance of the camera from the origin
        15              ------- HEIGHT            <- height of the camera with respect to the origin
        0;0;1           ------- TARGET            <- the center of the scene 
        0;0;1           ------- UP                <- direction of the UP vector
        0.1             ------- MOV               <- right-left increment when moving the scene
        0.1             ------- GAP               <- bottom-up increment when moving the scene
        0.1;0.1;0       ------- ENV_COLOR         <- background color
        0.6;0.2;0.2     ------- GROUND_COLOR      <- floor color
        0.2;0.6;0.2     ------- OBJECT_COLOR      <- generic object color 
        0.2;0.2;0.0     ------- BOX_COLOR         <- box color
        0.4;0.4;0.6     ------- CAPSULE_COLOR     <- capsule color
        0.7;0.1;0.7     ------- SPHERE_COLOR      <- sphere color
        0.2;0.6;0.2     ------- COMPOUND_COLOR    <- color of a compound object
        0;0.6;0.6       ------- SOFT_COLOR        <- color of a soft body
        0.5;.5;0.2;1    ------- LIGHT_AMBIENT     <- lights - ambient light color
        1;1;1;1         ------- LIGHT_DIFFUSE     <- lights - diffuse light color
        1;1;1;1         ------- LIGHT_SPECULAR    <- lights -specular light color
        10;10;10;1      ------- LIGHT_POSITION0   <- light0 position
        10;10;10;0      ------- LIGHT_POSITION1   <- light1 position
     \endverbatim
     *
     * physics_parameters 
     \verbatim
        0;0;-9.8        ------- GRAVITY           <- a vector defining gravity amplitude and direction
        0.05            ------- STEP              <- integration step for physics
        20              ------- SUBSTEP           <- number of real teps if timestep is too long
     \endverbatim
     *
     *
     * \subsection examples Examples
     *
     * These are some examples of how to implement the main features:
     * <ul>
     * <li>\ref joint
     * <li>\ref soft 
     * <li>\ref import 
     * <li>\ref motor 
     * <li>\ref camera
     * </ul>    
     *
     * \subsubsection joint Joints
     *
     * derive an Engine from CENSEngine:
     *
     * \includelineno joint/engine.h   
     * 
     * Implement the overloading of CENSEngine::initObjects and
     * CENSEngine::step 
     *
     * \includelineno joint/engine.cpp  
     * 
     * \subsubsection soft Soft bodies
     *
     * derive an Engine from CENSEngine:
     *
     * \includelineno soft/engine.h   
     * 
     * Implement the overloading of CENSEngine::initObjects and
     * CENSEngine::step 
     *
     * \includelineno soft/engine.cpp    
     *
     * <hr>
     * \subsubsection import Importing a bullet file
     *
     * derive an Engine from CENSSerializedEngine:
     *
     * \includelineno import/engine.h   
     * 
     * Now the overloading of CENSEngine::initObjects and
     * CENSEngine::step is very simple. Just call loadBulletFile:
     *
     * \includelineno import/engine.cpp    
     *
     * \subsubsection motor Controlling motors on an imported joint 
     *
     *
     * derive an Engine from CENSSerializedEngine:
     *
     * \includelineno motor/engine.h   
     * 
     * Within CENSEngine::initObjects call loadBulletFile, while 
     * within CENSEngine::step call the method CENSSerializedRobot::move 
     * on the acquired CENSSerializedRobot object, indicating one of the 
     * loaded hinges:
     *
     * \includelineno motor/engine.cpp    
     * 
     * \subsubsection camera Cameras
     *
     * derive an Engine from CENSSerializedEngine
     *
     * \includelineno camera/engine.h   
     * 
     * Implement the overloading of CENSSerializedEngine::initObjects and
         * CENSSerializedEngine::step 
     *
     * \includelineno camera/engine.cpp   
     * 
     * 
     **/

}

