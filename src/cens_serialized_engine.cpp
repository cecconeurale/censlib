// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2013 Francesco Mannella
//
// cens_engine.cpp
// Copyright (c) 2013 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#include "cens_utils.h"
#include "cens_serialized_engine.h"

namespace cens 
{

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////


    void CENSSerializedRobot::addHinge( 
            std::string hingeName, CENSHingeConstraint *_hinge )
    {
        // initialize the data struct
        hinges[hingeName] = CENSHingeData();
        // link the pointer of the hinge
        hinges[hingeName].hinge = _hinge;

        // set Parameters to be read and written from/to
        // the file
        addParameter(std::string(hingeName)
                .append("_ENABLED"), 
                hinges[hingeName].enabled );
        addParameter(std::string(hingeName)
                .append("_MOTOR_TYPE"), 
                hinges[hingeName].motor_type );
        addParameter(std::string(hingeName)
                .append("_KP"), 
                hinges[hingeName].kp );
        addParameter(std::string(hingeName)
                .append("_KD"), 
                hinges[hingeName].kd );
        addParameter(std::string(hingeName)
                .append("_MAX_MOTOR_IMPULSE"), 
                hinges[hingeName].max_motor_impulse );
        addParameter(std::string(hingeName)
                .append("_MOTOR_DT"), 
                hinges[hingeName].motor_dt );
        addParameter(std::string(hingeName)
                .append("_CURRENT_ANGLE"), 
                hinges[hingeName].current_angle );
        addParameter(std::string(hingeName)
                .append("_LOWER_LIMIT"), 
                hinges[hingeName].lower_limit );
        addParameter(std::string(hingeName)
                .append("_UPPER_LIMIT"), 
                hinges[hingeName].upper_limit );
        addParameter(std::string(hingeName)
                .append("_ENABLE_COLLISION"), 
                hinges[hingeName].enable_collision );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSSerializedRobot::addGenericConstraint( std::string constraintName, 
            btTypedConstraint *_constraint )
    {
        constraints[constraintName] = _constraint;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSSerializedRobot::addBody( std::string bodyName, btRigidBody *_body )
    {
        bodies[bodyName] = _body;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSSerializedRobot::addTransform( 
            std::string transformName, btTransform _transform )
    {
        transforms[transformName] = _transform;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    bool CENSSerializedRobot::init()
    {
        bool ret =  loadParameters();
        for(
                Hinges::iterator it = hinges.begin();
                it!=hinges.end();
                ++it )
        {
            CENSHingeData &hdata = it->second;
            hdata.hinge->setLimit(hdata.lower_limit, hdata.upper_limit);
            hdata.hinge->setMaxMotorImpulse(hdata.max_motor_impulse);
            hdata.hinge->enableMotor(hdata.enabled=="TRUE");
            if (hdata.enable_collision=="FALSE")
            {     
                CENSNonCollidingTable table;
                table[std::make_pair(
                        &(hdata.hinge->getRigidBodyA()),
                        &(hdata.hinge->getRigidBodyB()))] = false;
                engine->setCollisionFilter(table);
            } 
            else
            {
                CENSNonCollidingTable table;
                table[std::make_pair(
                        &(hdata.hinge->getRigidBodyA()),
                        &(hdata.hinge->getRigidBodyB()))] = true;
                engine->setCollisionFilter(table);
            }

        }

        saveParameters();
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSSerializedRobot::disableBodies()
    {
        for(Bodies::iterator it = bodies.begin(); it!= bodies.end(); ++it)
        {
            btRigidBody *body = it->second;
            std::string name = it->first;
            body->setActivationState(DISABLE_SIMULATION);
            body->setCenterOfMassTransform(transforms[name]);
            body->setLinearVelocity(btVector3(0,0,0));
            body->setAngularVelocity(btVector3(0,0,0));
            body->setGravity(btVector3(0,0,0));
        }
        for(Hinges::iterator it = hinges.begin(); it!= hinges.end(); ++it)
        {
            std::string hname = it->first;
            move(hname,0,0);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSSerializedRobot::enableBodies()
    {
        for(Bodies::iterator it = bodies.begin(); it!= bodies.end(); ++it)
        {
            btRigidBody *body = it->second;
            std::string name = it->first;
            body->forceActivationState(DISABLE_DEACTIVATION);
            body->setGravity(eigen2btVec(engine->getGravity()));
        }
    }
 
    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSSerializedRobot::update()
    {
        for(
                Hinges::iterator it = hinges.begin();
                it!=hinges.end();
                ++it )
        {
            CENSHingeData &hdata = it->second;

            btDynamicsWorld  *world = engine->getDynamicsWorld();
            bool is_in_world = false;
            for(int x =0; x<world->getNumConstraints();x++)
                if(world->getConstraint(x) == hdata.hinge)
                {
                    is_in_world = true;
                    break;
                }

            if(is_in_world)
            {
                hdata.hinge->setLimit(hdata.lower_limit, hdata.upper_limit );

                if( hdata.enabled=="TRUE")
                {
                    hdata.hinge->setMaxMotorImpulse(hdata.max_motor_impulse);
                    hdata.hinge->enableMotor(hdata.enabled=="TRUE");
                    if(hdata.motor_type == CENS_DEFAULT_CONTROL)
                        hdata.hinge->setMotorTarget(
                                hdata.current_angle, 
                                hdata.motor_dt);
                    else if(hdata.motor_type == CENS_PD_CONTROL)
                        hdata.hinge->setPDMotorTarget(
                                hdata.current_angle, 
                                hdata.kp, 
                                hdata.kd, 
                                hdata.motor_dt);
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSSerializedRobot::move(
            const std::string hingeName, double target_angle, 
            double impulse, double prop)
    {
        if (impulse != 0)
        {
            if(hinges[hingeName].motor_type == CENS_DEFAULT_CONTROL)
            {
                hinges[hingeName].max_motor_impulse = impulse;  
            } 
            else if (hinges[hingeName].motor_type == CENS_PD_CONTROL)
            {
                hinges[hingeName].kp = impulse;  
                hinges[hingeName].kd = impulse*prop;

            }
        }
        hinges[hingeName].current_angle = target_angle;
    }

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

void CENSSerializedEngine::step(int timestep)
{
    // update hinge positions of all hinge groups on every step 
    for(Robots::iterator it = m_eRobots.begin(); it!=m_eRobots.end(); ++it)
    {
        it->second->update();
    }
    CENSEngine::step(timestep);
}

///////////////////////////////////////////////////////////////////////////////////////////////

CENSSerializedRobot *CENSSerializedEngine::loadBulletFile(   
        const std::string &robotName, 
        const std::string &file)
    {
        
        // build the hinge group for this bullet file
        CENSSerializedRobot *robot = new CENSSerializedRobot(robotName, this);

        // build the file importer
        CENSImporter *fileLoader = 
            new CENSImporter(m_phDynamicsWorld);
        fileLoader->loadFile(file.c_str());

        // get the map of bodies to theri names
        typedef CENSHashMap<btHashString,btRigidBody*> bhm;
        bhm &bm = (bhm &)(fileLoader->getBodyMap());

        // link bodies to CENS cicle
        for(int index=0; index<bm.size(); index++ )
        {
            btRigidBody *body = *(bm.getAtIndex(index));
            localImportRigidBody(body);
            m_eBodies[ bm.getKeyAtIndex(index).m_string ] = body;
            robot->addBody( bm.getKeyAtIndex(index).m_string, body );
            robot->addTransform( bm.getKeyAtIndex(index).m_string, 
                    body->getCenterOfMassTransform() );
        }

        // link constraints to CENS cicle
        for(int index=0; index<fileLoader->getNumConstraints(); index++ )
        {
            // get a constraint pointer
            btTypedConstraint *constraint = fileLoader->getConstraintByIndex(index);
 
            // Create the constraint name from the names of the bodies
            std::stringstream constr_name_stream;
            constr_name_stream << 
                fileLoader->getNameForPointer(&(constraint->getRigidBodyA()))
                << "_to_" <<
                fileLoader->getNameForPointer(&(constraint->getRigidBodyB()));
       
            // if hinge serialize
            if(constraint->getConstraintType() == HINGE_CONSTRAINT_TYPE )
            {
                // migrade from a btHingeconstraint to a CENSHingeConstraint
                btHingeConstraint *hinge = dynamic_cast< btHingeConstraint *>(constraint);
                CENSHingeConstraint *cens_constraint = new CENSHingeConstraint(
                        hinge->getRigidBodyA(),
                        hinge->getRigidBodyB(),
                        hinge->getFrameOffsetA(),
                        hinge->getFrameOffsetB() );
                hinge->getRigidBodyA().addConstraintRef(cens_constraint);
                hinge->getRigidBodyB().addConstraintRef(cens_constraint);
                // substitute the constraint in the dynamics
                m_phDynamicsWorld->removeConstraint(constraint);
                m_phDynamicsWorld->addConstraint(cens_constraint);
                delete constraint;
                constraint = cens_constraint; 

                // add the constraint to the serialization group
                robot->addHinge(constr_name_stream.str(), cens_constraint);
            }
            else
            {
                robot->addGenericConstraint(constr_name_stream.str(), constraint);
            }

            // add the constraint to the CENS constraints' map
            m_eConstraints[constr_name_stream.str()] = constraint;
        }
 
        // delete data in the importer
        delete fileLoader;

        // reset gravity as from CENS parameters
        m_phDynamicsWorld->setGravity(
                btVector3(
                    m_phGravity.x(), 
                    m_phGravity.y(),
                    m_phGravity.z()) );
        m_phSoftBodyWorldInfo.m_gravity.setValue(
                m_phGravity.x(), 
                m_phGravity.y(), 
                m_phGravity.z() );

        // initialize and return the serialized hinge group
        robot->init();
        return robot;
    }

}
