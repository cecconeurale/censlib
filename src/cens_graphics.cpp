// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_graphics.cpp
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#include <iomanip>
#include <sstream>
#include "cens_graphics.h"
#include "cens_utils.h"

using namespace Magick;

namespace cens {

  ///////////////////////////////////////////////////////////////////////////////////////////////
  //// CALLBACKS ////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void glutKeyboardCallback(unsigned char key, int x, int y)
  {
    cens_graphics->keyboard(key,x,y);
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void glutDisplayCallback(void)
  {
    cens_graphics->display();
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void glutTimerCallback( int value )
  {
    cens_graphics->step(value);
    if( value ) glutPostRedisplay();
    glutTimerFunc(value,glutTimerCallback,value);
  }


  ///////////////////////////////////////////////////////////////////////////////////////////////
  //// INITIALIZATION ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  CENSGraphics* cens_graphics = 0;

  ///////////////////////////////////////////////////////////////////////////////////////////////

  CENSGraphics::~CENSGraphics() {

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////
  //// PUBLIC METHODS ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::initCENSGraphics( int argc, char ** argv ) 
  {

    ////////////////////////////////////////////////////////////
    // PARAMETERS //////////////////////////////////////////////

    m_grHide=false;
    if (argc==2) {
      if( std::string(argv[1]).compare("--hide")==0 ) {
        m_grHide = true;
      }
    }

    // INITIALIZE ////////////////////////////////////

    m_grArgc = argc;
    m_grArgv = argv;


    /// Parameters from file 


    ////////////////////////////////////////////////////////////
    // VIEWPOINT ///////////////////////////////////////////////

    m_grParameterManager.addParameter("SCREEN_WIDTH",m_grViewCamera.m_cScreenWidth);
    m_grParameterManager.addParameter("SCREEN_HEIGHT",m_grViewCamera.m_cScreenHeight);
    m_grParameterManager.addParameter("SCREEN_XGAP",m_grViewCamera.m_cScreenXGap);
    m_grParameterManager.addParameter("SCREEN_YGAP",m_grViewCamera.m_cScreenYGap);
    m_grParameterManager.addParameter("TIMESTEP",m_grTimestep);
    m_grParameterManager.addParameter("SCREEN_TITLE",m_grViewCamera.m_cScreenTitle);
    m_grParameterManager.addParameter("FIELD_OF_VIEW",m_grViewCamera.m_cFoV);
    m_grParameterManager.addParameter("RATIO",m_grViewCamera.m_cRatio);
    m_grParameterManager.addParameter("NEAR",m_grViewCamera.m_cNear);
    m_grParameterManager.addParameter("FAR",m_grViewCamera.m_cFar);
    m_grParameterManager.addParameter("ANGLE",m_grViewCamera.m_cAngle);
    m_grParameterManager.addParameter("DISTANCE",m_grViewCamera.m_cDistance);
    m_grParameterManager.addParameter("HEIGHT",m_grViewCamera.m_cHeight);
    m_grParameterManager.addParameter("TARGET",m_grViewCamera.m_cTarget);
    m_grParameterManager.addParameter("UP",m_grViewCamera.m_cUp);
    m_grParameterManager.addParameter("MOV",m_grViewCamera.m_cMov);
    m_grParameterManager.addParameter("GAP",m_grViewCamera.m_cGap);

    
    // ////////////////////////////////////////////////////////////
    // // COLORS //////////////////////////////////////////////////
    
    m_grParameterManager.addParameter("ENV_COLOR",m_grEnvColor);
    m_grParameterManager.addParameter("GROUND_COLOR",m_grGroundColor);
    m_grParameterManager.addParameter("OBJECT_COLOR",m_grObjectColor);
    m_grParameterManager.addParameter("BOX_COLOR",m_grBoxColor);
    m_grParameterManager.addParameter("CAPSULE_COLOR",m_grCapsuleColor);
    m_grParameterManager.addParameter("SPHERE_COLOR",m_grSphereColor);
    m_grParameterManager.addParameter("COMPOUND_COLOR",m_grCompoundColor);
    m_grParameterManager.addParameter("SOFT_COLOR",m_grSoftColor);
    
    // ///////////////////////////////////////////////////////////
    // // LIGHTS /////////////////////////////////////////////////
    
    m_grParameterManager.addParameter("LIGHT_AMBIENT", m_grLight.m_lAmbient);
    m_grParameterManager.addParameter("LIGHT_DIFFUSE", m_grLight.m_lDiffuse);
    m_grParameterManager.addParameter("LIGHT_SPECULAR", m_grLight.m_lSpecular);
    m_grParameterManager.addParameter("LIGHT_POSITION0", m_grLight.m_lPosition0);
    m_grParameterManager.addParameter("LIGHT_POSITION1", m_grLight.m_lPosition1);

    ////////////////////////////////////////////////////////////

    if (!m_grParameterManager.loadParameters())
    {

        m_grViewCamera.m_cScreenWidth = 800;
        m_grViewCamera.m_cScreenHeight = 600;
        m_grViewCamera.m_cScreenXGap = 10;
        m_grViewCamera.m_cScreenYGap = 10;
        m_grTimestep = 10;
        m_grViewCamera.m_cScreenTitle = "Demo";
        m_grViewCamera.m_cFoV = 70.0;
        m_grViewCamera.m_cRatio = 800/600;
        m_grViewCamera.m_cNear = 1.0;
        m_grViewCamera.m_cFar = 10000.0;
        m_grViewCamera.m_cAngle = 0.75;
        m_grViewCamera.m_cDistance = 85.0;
        m_grViewCamera.m_cHeight = 4.5;
        m_grViewCamera.m_cTarget << 0.0, 0.0, 1.0;
        m_grViewCamera.m_cUp << 0.0, 0.0, 1.0;
        m_grViewCamera.m_cMov = 45.0;
        m_grViewCamera.m_cGap = 0.2;


        // ////////////////////////////////////////////////////////////
        // // COLORS //////////////////////////////////////////////////

        m_grEnvColor << 0.1, 0.1, 0.0;
        m_grGroundColor << 0.6, 0.2, 0.2;
        m_grObjectColor << 0.2, 0.6, 0.2;
        m_grBoxColor << 0.8, 0.8, 0.2;
        m_grCapsuleColor << 0.4, 0.4, 0.6;
        m_grSphereColor << 0.7, 0.1, 0.7;
        m_grCompoundColor << 0.2, 0.6, 0.2;
        m_grSoftColor << 0.0, 0.6, 0.0;

        // ///////////////////////////////////////////////////////////
        // // LIGHTS /////////////////////////////////////////////////

        m_grLight.m_lAmbient << 0.2, 0.2, 0.2, 1.0;
        m_grLight.m_lDiffuse << 1.0, 1.0, 1.0, 1.0;
        m_grLight.m_lSpecular << 1.0, 1.0, 1.0, 1.0;
        m_grLight.m_lPosition0 << 1.0, 10.0, 1.0, 0.0;
        m_grLight.m_lPosition1 << -1.0, -10.0, -1.0, 0.0;

        m_grParameterManager.saveParameters();
    }
    
    ////
    
    glutInit( &(m_grArgc), m_grArgv);


    ////////////////////////////////////////////////////////////
    // SCREEN //////////////////////////////////////////////////

    // mainCamera
    initCamera(m_grViewCamera,m_grLight);
    glutKeyboardFunc(glutKeyboardCallback);
    glutTimerFunc(0,glutTimerCallback, m_grTimestep);

    ////////////////////////////////////////////////////////////

    m_grAxisEnabled = false;
    m_grSimEnabled = true;
    m_grTextureEnabled = false;
    m_grObjectAxesEnabled = false;
    m_grJointAxesEnabled = false;
    m_grCameraAxisEnabled = false;

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::loop( ) 
  {
        glutMainLoop();
  }

  void CENSGraphics::stepToStepLoop( ) 
  {
        glutMainLoopEvent ();
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::step( int value )
  {
    for(unsigned int x=0; x<m_grCameras.size(); x++) {
      if(x > 0 || (not isIdle()) ) {
        focusCamera(x);   
        beginRendering();
        updateCamera(x);
        display();
        endRendering();
      }
    }
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::display( )
  {

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  bool CENSGraphics::isIdle() 
  {  
    return m_grHide;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::quit() 
  {  
    glutLeaveMainLoop(); 
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::keyboard( unsigned char key, int x, int y )
  {

    switch (key) {
      case 'q' : quit(); break;
      case 'l' : stepLeft(); break;
      case 'j' : stepRight(); break;
      case 'i' : stepFront(); break;
      case 'm' : stepBack(); break;
      case '4' : stepLeft(); break;
      case '6' : stepRight(); break;
      case '8' : stepFront(); break;
      case '2' : stepBack(); break;
      case '7' : stepLeft(); stepFront(); break;
      case '9' : stepRight(); stepFront(); break;               
      case '1' : stepLeft(); stepBack(); break;
      case '3' : stepRight(); stepBack(); break; 
      case 'z' : zoomIn(); break;
      case 'x' : zoomOut(); break;      
      case 't' : toggleTexture(); break;      
      case 'a' : toggleAxis(); break;      
      case 'd' : toggleObjectAxes(); break;      
      case 'f' : toggleJointAxes(); break;      
      case 's' : toggleCameraAxis(); break;      
      case 'w' : toggleStop(); break;      
      default : break;
    }

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////
  //// SERVICE METHODS //////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::beginRendering() 
  {

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);		/* clear the display */

  }

  //////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::endRendering() 
  {
    glutSwapBuffers();
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::initTextures( ) {

    m_grTextureInitialized=true;

    for(unsigned int i=0; i<m_grShapes.size();i++)
      m_grShapes[i]->initTexture();
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::initTexture( const GLubyte *source, int width, int height ) {

    m_grTextureInitialized=true;

    GLubyte *image;

    if(source==0) {

      width = 256;
      height = 256;
      image = new GLubyte[256*256*3];
      for(int y=0;y<256;++y)
      {
        const int	t=y>>4;
        GLubyte*pi=image+y*256*3;
        for(int x=0;x<256;++x)
        {
          const int	s=x>>4;
          const GLubyte	b=180;
          GLubyte	c = b+(((s+t)&1)&1)*(255-b);
          pi[0]=pi[1]=pi[2]=c;pi+=3;
        }
      }

    } else {

      image = new GLubyte[width*height*3];
      for(int x=0;x<width*height*3; x++) {
        image[x] = source[x];
      }

    }

    // texture init
    GLuint texture_id;

    glGenTextures( 1, &texture_id );
    glBindTexture( GL_TEXTURE_2D, texture_id );

    if(image)
      gluBuild2DMipmaps(
          GL_TEXTURE_2D,3,width,height,
          GL_RGB,GL_UNSIGNED_BYTE,
          image);

    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

    delete image;
    
    glEnable( GL_TEXTURE_2D );

  }

  //////////////////////////////////////////////////////////////////////////////////////////////
  // CAMERA METHODS ////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////

  void  CENSGraphics::initCamera(CENSCamera &camera, CENSLight light) {

    m_grCameras.push_back(&camera);
    camera.initPixels();

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | 
        GLUT_DEPTH | GLUT_STENCIL);
    glutInitWindowSize(camera.m_cScreenWidth, camera.m_cScreenHeight);
    glutInitWindowPosition(camera.m_cScreenXGap, camera.m_cScreenYGap);
    camera.m_cHandle = glutCreateWindow(camera.m_cScreenTitle.c_str());
    if(m_grHide) { 
      glutHideWindow();
      glutIconifyWindow();
    }

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(
        camera.m_cFoV,          
        camera.m_cRatio,           
        camera.m_cNear,  
        camera.m_cFar);  

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    ////////////////////////////////////////////////////////////
    // OPENGL 3D ENABLING //////////////////////////////////////

    glEnable(GL_DEPTH_TEST);
    glClearDepth(1.0f);
    glDepthFunc(GL_LESS);
    glShadeModel(GL_SMOOTH);
    glLineWidth(1.0);
    glPointSize(1.0);
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
    glEnable(GL_CULL_FACE);
    glClearColor(m_grEnvColor(0),m_grEnvColor(1),m_grEnvColor(2),1.0);           
    glEnable(GL_COLOR_MATERIAL);

    ////////////////////////////////////////////////////////////
    // LIGHTING ////////////////////////////////////////////////
    
    GLfloat m_lAmbient[] = {
      light.m_lAmbient[0],light.m_lAmbient[1],
      light.m_lAmbient[2],light.m_lAmbient[3] };

    GLfloat m_lDiffuse[] = {
      light.m_lDiffuse[0],light.m_lDiffuse[1],
      light.m_lDiffuse[2],light.m_lDiffuse[3] };

    GLfloat m_lSpecular[] = {
      light.m_lSpecular[0],light.m_lSpecular[1],
      light.m_lSpecular[2],light.m_lSpecular[3] };

    GLfloat m_lPosition0[] = {
      light.m_lPosition0[0],light.m_lPosition0[1],
      light.m_lPosition0[2],light.m_lPosition0[3] };

    GLfloat m_lPosition1[] = {
      light.m_lPosition1[0],light.m_lPosition1[1],
      light.m_lPosition1[2],light.m_lPosition1[3] };

    glLightfv(GL_LIGHT0, GL_AMBIENT, m_lAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, m_lDiffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, m_lSpecular);
    glLightfv(GL_LIGHT0, GL_POSITION, m_lPosition0);

    glLightfv(GL_LIGHT1, GL_AMBIENT, m_lAmbient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, m_lDiffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, m_lSpecular);
    glLightfv(GL_LIGHT1, GL_POSITION, m_lPosition1);

 
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    //glEnable(GL_LIGHT1);

    //  //////////////////////////////////////////////////////////////
    //
    //  initTextures();

    /////////////////////////////////////////////////////////////

    glutDisplayFunc(glutDisplayCallback);

  }

  //////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::focusCamera( int camera_index ) {

    CENSCamera &camera = *m_grCameras[camera_index];

    glutSetWindow(camera.m_cHandle);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

  }

  //////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::updateCamera( int camera_index) {

    CENSCamera &camera = *m_grCameras[camera_index];

    // reset the viewpoint of all implemented cameras

    if(&camera == &m_grViewCamera) {

      gluLookAt( 

          camera.m_cDistance*cos(camera.m_cAngle),
          camera.m_cDistance*sin(camera.m_cAngle),
          camera.m_cHeight,

          camera.m_cTarget(0),
          camera.m_cTarget(1),
          camera.m_cTarget(2),

          camera.m_cUp(0),
          camera.m_cUp(1),
          camera.m_cUp(2) );
    }
    else 
      gluLookAt( 

          camera.m_cOrigin(0),
          camera.m_cOrigin(1),
          camera.m_cOrigin(2),

          camera.m_cTarget(0),
          camera.m_cTarget(1),
          camera.m_cTarget(2),

          camera.m_cUp(0),
          camera.m_cUp(1),
          camera.m_cUp(2) );

    glutPostRedisplay();

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  const CENSPixelMap &CENSGraphics::getCameraPixelMap( int camera_index ) {

    CENSCamera &camera = *m_grCameras[camera_index];
   
    int data_length =
      camera.m_cScreenWidth * 
      camera.m_cScreenHeight * 3 ;
    unsigned char *pixels = 
      new unsigned char[ data_length ];

    glutSetWindow(camera.m_cHandle);
    glReadPixels(
        1,1,camera.m_cScreenWidth,
        camera.m_cScreenHeight,
        GL_RGB,GL_BYTE,pixels);
    glutSwapBuffers(); 
    glutSetWindow(m_grViewCamera.m_cHandle);
  
    camera.m_cPixmap.getBlob()
      .updateNoCopy(pixels,data_length);


    return camera.m_cPixmap;

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::saveCameraPixelMap( int camera_index, int t, const char * type ) {

    CENSCamera &camera = *m_grCameras[camera_index];
 
    int data_length =
      camera.m_cScreenWidth * 
      camera.m_cScreenHeight * 4;
    unsigned int *pixels = 
      new unsigned int[ data_length ];

    glutSetWindow(camera.m_cHandle);
    glReadPixels(
        1,1,camera.m_cScreenWidth,
        camera.m_cScreenHeight,
        GL_RGBA,GL_UNSIGNED_BYTE,pixels);
    glutSwapBuffers(); 
    glutSetWindow(m_grViewCamera.m_cHandle);
  
    camera.m_cPixmap.getBlob()
      .updateNoCopy(pixels,data_length);

    Image image( 
        camera.m_cScreenWidth,
        camera.m_cScreenHeight, 
        "RGBA",
        CharPixel,
        pixels );
    image.flip();

    std::stringstream filename;
    filename 
      << std::setfill('0') << std::setw(3)
      << camera.m_cHandle
      << "_"
      << std::setfill('0') << std::setw(8)
      << t
      << "." << type;
    image.write(filename.str().c_str());

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////
  //// RENDERING METHODS ////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::drawPlane( const Vector3f& orig, const Vector3f& vec0, const Vector3f& vec1  ) 
  {

    glPushMatrix();

    glColor3f(
        m_grGroundColor(0),
        m_grGroundColor(1),
        m_grGroundColor(2));

    float vecLen = 20.f;
    Vector3f pt0 = orig + vec0*vecLen;
    Vector3f pt1 = orig - vec0*vecLen;
    Vector3f pt2 = orig + vec1*vecLen;
    Vector3f pt3 = orig - vec1*vecLen;

    glBegin(GL_QUADS);

    glVertex3f(pt0.x(),pt0.y(),pt0.z());
    glVertex3f(pt2.x(),pt2.y(),pt2.z());
    glVertex3f(pt1.x(),pt1.y(),pt1.z());
    glVertex3f(pt3.x(),pt3.y(),pt3.z());

    glEnd();

    glPopMatrix();

  }
  ///////////////////////////////////////////////////////////////////////////////////////////////


  void CENSGraphics::drawBox( 
      const Vector3f& org, const Matrix3f& rotMat, const Vector3f& halfExtent  ) 
  {
    // triples of vertice indexes
    static int indices[12][3] = {
      {0,1,2},
      {3,2,1},
      {4,0,6},
      {6,0,2},
      {5,1,4},
      {4,1,0},
      {7,3,1},
      {7,1,5},
      {5,4,7},
      {7,4,6},
      {7,2,3},
      {7,6,2} };


    // triples of texture coordinates for each triangle
    static int tex_coords[12][6] = {
      {1,1,0,1,1,0},
      {0,0,1,0,0,1},
      {1,1,0,1,1,0},
      {1,0,0,1,0,0},
      {1,1,0,1,1,0},
      {1,0,0,1,0,0},
      {1,1,0,1,0,0},
      {0,0,1,1,1,0},
      {1,0,1,1,0,0},
      {0,0,1,1,1,0},
      {1,0,0,1,0,0},
      {0,1,1,1,1,0} };


    // defining half extents 

    Vector3f dx(rotMat(0,0),rotMat(0,1),rotMat(0,2));
    Vector3f dy(rotMat(1,0),rotMat(1,1),rotMat(1,2));
    Vector3f dz(rotMat(2,0),rotMat(2,1),rotMat(2,2));

    dx *= halfExtent[0];
    dy *= halfExtent[1];
    dz *= halfExtent[2];

    // defining vertices coordinates

    Vector3f vertices[8]={
      org+dx+dy+dz, 
      org-dx+dy+dz, 
      org+dx-dy+dz, 	
      org-dx-dy+dz, 	
      org+dx+dy-dz, 
      org-dx+dy-dz, 	
      org+dx-dy-dz, 	
      org-dx-dy-dz };

    // rendering

    glPushMatrix();

    glColor3f(
        m_grBoxColor(0),
        m_grBoxColor(1),
        m_grBoxColor(2));

    glBegin(GL_TRIANGLES);

    int si=12;
    for (int i=0;i<si;i++)
    {

      const Vector3f& v1 = vertices[indices[i][0]];;
      const Vector3f& v2 = vertices[indices[i][1]];
      const Vector3f& v3 = vertices[indices[i][2]];

      // enable reflection on the triangle

      Vector3f normal = (v3-v1).cross(v2-v1);
      normal.normalize ();
      glNormal3f(normal.x(),normal.y(),normal.z());

      // draws a triangle

      glTexCoord2f(tex_coords[i][0],tex_coords[i][1]);
      glVertex3f (v1.x(), v1.y(), v1.z());
      glTexCoord2f(tex_coords[i][2],tex_coords[i][3]);
      glVertex3f (v2.x(), v2.y(), v2.z());
      glTexCoord2f(tex_coords[i][4],tex_coords[i][5]);
      glVertex3f (v3.x(), v3.y(), v3.z());

    }
    glEnd();

    glPopMatrix();

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::drawSphere( const Vector3f& org, const Matrix3f& rot, float radius ) {

    int i, j;
    int lats = 20;
    int longs = 20;

    glPushMatrix();

    glColor3f(
        m_grSphereColor(0),
        m_grSphereColor(1),
        m_grSphereColor(2));

    float xtag = 1/(float)lats;
    float ytag = 1/(float)longs;

    for(i = 0; i <= lats; i++) {

      float lat0 = M_PI * (-float(0.5) +  (i - 1) * xtag);
      float z0 = radius*sin(lat0);
      float zr0 = radius*cos(lat0);

      float lat1 = M_PI * (-float(0.5) +  i * xtag);
      float z1 = radius*sin(lat1);
      float zr1 = radius*cos(lat1);

      glBegin(GL_QUAD_STRIP);
      for(j = 0; j <= longs; j++) {
        float lng = 2 * M_PI * (j - 1) * ytag;
        float x = cos(lng);
        float y = sin(lng);

        Vector3f ver1(x * zr1, y * zr1, z1);
        Vector3f ver2(x * zr0, y * zr0, z0);

        ver1 = org + rot*ver1;
        ver2 = org + rot*ver2;

        glNormal3f(ver1.x(),ver1.y(),ver1.z());
        glTexCoord2f(xtag*( i     ), ytag*( j     ));
        glVertex3f(ver1.x(),ver1.y(),ver1.z());

        glNormal3f(ver2.x(),ver2.y(),ver2.z());
        glTexCoord2f(xtag*( i - 1 ), ytag*( j     ));
        glVertex3f(ver2.x(),ver2.y(),ver2.z());

      }
      glEnd();

    }

    glPopMatrix();

  }
  
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::drawLine(const Vector3f &v1,const Vector3f &v2, const Vector3f color) {

    glPushMatrix();

    glColor3f( color.x(), color.y(),color.z());

    glBegin(GL_LINES);

    // draws a line

    glVertex3f (v1.x(), v1.y(), v1.z());
    glVertex3f (v2.x(), v2.y(), v2.z());

    glEnd();

    glPopMatrix();

  }



  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::drawTriangle(const Vector3f &v1,const Vector3f &v2,const Vector3f &v3, const Vector3f n) {

    glPushMatrix();

    glColor3f(
        .4,
        .6,
        .2);

    glBegin(GL_TRIANGLES);

    // enable reflection on the triangle

    glNormal3f(n.x(),n.y(),n.z());

    // draws a triangle

    glVertex3f (v1.x(), v1.y(), v1.z());
    glVertex3f (v2.x(), v2.y(), v2.z());
    glVertex3f (v3.x(), v3.y(), v3.z());


    glEnd();

    glPopMatrix();

  }


  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::drawConvex( const Vector3f& org, const Matrix3f& rot, 
      const std::vector<Vector3f> &vtx, const unsigned int *idx, int nvtxs, int nidxs, int ntrns ) {

    if (ntrns > 0)
    {
      int index = 0;

      glPushMatrix();

      glColor3f(
          m_grCapsuleColor(0),
          m_grCapsuleColor(1),
          m_grCapsuleColor(2));

      glBegin(GL_TRIANGLES);

      for (int i = 0; i < ntrns; i++)
      {

        int i1 = index++;
        int i2 = index++;
        int i3 = index++;

        assert(i1 < nidxs && 
            i2 < nidxs 
            && i3 < nidxs);

        int index1 = idx[i1];
        int index2 = idx[i2];
        int index3 = idx[i3];
        assert(index1 < nvtxs &&
            index2 < nvtxs &&
            index3 < nvtxs );

        Vector3f v1 = org + rot*vtx[index1];
        Vector3f v2 = org + rot*vtx[index2];
        Vector3f v3 = org + rot*vtx[index3];

        Vector3f axis = (v3-v1).cross(v2-v1);
        axis.normalize ();

        glNormal3f(axis.x(),axis.y(),axis.z());

        float div=10.;

        glTexCoord2f(v1.x()/div,v1.z()/div);
        glVertex3f (v1.x(), v1.y(), v1.z());
        glTexCoord2f(v2.x()/div,v2.z()/div);
        glVertex3f (v2.x(), v2.y(), v2.z());
        glTexCoord2f(v3.x()/div,v3.z()/div);
        glVertex3f (v3.x(), v3.y(), v3.z());

      }
      glEnd();

      glPopMatrix();

    }

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::drawAxis(const Vector3f &origin, const Matrix3f &rotation, float length) {

    Vector3f o = origin;
    Vector3f x = origin + rotation*Vector3f(length,0,0);
    Vector3f y = origin + rotation*Vector3f(0,length,0);
    Vector3f z = origin + rotation*Vector3f(0,0,length);

    glPushMatrix();

    glLineWidth(2.0);

    glBegin(GL_LINES);

    glColor3f(1,0,0);
    glVertex3f (o.x(), o.y(), o.z());
    glVertex3f (x.x(), x.y(), x.z());

    glColor3f(0,1,0);
    glVertex3f (o.x(), o.y(), o.z());
    glVertex3f (y.x(), y.y(), y.z());

    glColor3f(0,0,1);
    glVertex3f (o.x(), o.y(), o.z());
    glVertex3f (z.x(), z.y(), z.z());

    glEnd();

    glLineWidth(1.0);

    glPopMatrix();

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////
  //// KEYBOARD METHODS /////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::stepLeft() 
  {
    m_grViewCamera.m_cAngle -= m_grViewCamera.m_cMov;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::stepRight() 
  {
    m_grViewCamera.m_cAngle += m_grViewCamera.m_cMov;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::stepFront()  
  {
    m_grViewCamera.m_cHeight += 4*m_grViewCamera.m_cGap;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::stepBack() 
  {

    m_grViewCamera.m_cHeight -= 4*m_grViewCamera.m_cGap;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::zoomIn() 
  {
    m_grViewCamera.m_cDistance -= 4*m_grViewCamera.m_cGap;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::zoomOut() 
  {
    m_grViewCamera.m_cDistance += 4*m_grViewCamera.m_cGap;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::toggleTexture() 
  {
    if(m_grTextureEnabled) {

      for(unsigned int i=0; i<m_grEyeCameras.size(); i++) {
        glutSetWindow(m_grEyeCameras[i]->m_cHandle); 
        glDisable( GL_TEXTURE_2D );
      }

      // reset the main viewpoint
      glutSetWindow(m_grViewCamera.m_cHandle); 
      glDisable( GL_TEXTURE_2D );

      m_grTextureEnabled = false;

    } else {
      for(unsigned int i=0; i<m_grEyeCameras.size(); i++) {
        glutSetWindow(m_grEyeCameras[i]->m_cHandle); 
        glEnable( GL_TEXTURE_2D );
      }

      // reset the main viewpoint
      glutSetWindow(m_grViewCamera.m_cHandle); 
      glEnable( GL_TEXTURE_2D );

      m_grTextureEnabled = true;
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::toggleAxis() 
  {
    if(m_grAxisEnabled) {
      m_grAxisEnabled = false;
    } else {
      m_grAxisEnabled = true;
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::toggleObjectAxes() 
  {
    if(m_grObjectAxesEnabled) {
      m_grObjectAxesEnabled = false;
    } else {
      m_grObjectAxesEnabled = true;
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::toggleJointAxes() 
  {
    if(m_grJointAxesEnabled) {
      m_grJointAxesEnabled = false;
    } else {
      m_grJointAxesEnabled = true;
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::toggleCameraAxis() 
  {
    if(m_grCameraAxisEnabled) {
      m_grCameraAxisEnabled = false;
    } else {
      m_grCameraAxisEnabled = true;
    }
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////

  void CENSGraphics::toggleStop() 
  {
    if(m_grSimEnabled) {
      m_grSimEnabled = false;
    } else {
      m_grSimEnabled = true;
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////

}
