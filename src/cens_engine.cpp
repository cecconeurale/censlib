// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_engine.cpp
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#include "cens_utils.h"
#include "cens_engine.h"
#include <vector>
#include <map>
#include <string>
#include <sstream>

namespace cens 
{

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    CENSEngine::CENSEngine() 
    {
        cens_graphics = this;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSEngine::init( int argc, char **argv  ) 
    {

        initCENSGraphics(argc, argv );
        initObjects();
        for(unsigned int x=0; x<m_grCameras.size(); x++) {
            focusCamera(x);   
            initTextures();
        }

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    CENSEngine::~CENSEngine()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSEngine::step( int value ) 
    {

        if(m_grSimEnabled)
            cens_physics_step( ); 

        CENSGraphics::step( value );

    }


    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSEngine::run() 
    {
        loop();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSEngine::stepRun() 
    {
        stepToStepLoop();
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSEngine::display() 
    {

        /////////////////////////////////////////////////////////
        // RENDER OBJECTS ///////////////////////////////////////

        btScalar	m[16];
        btVector3 aabbMin,aabbMax;
        btMatrix3x3	rot;
        btVector3	orig;
        rot.setIdentity();
        btQuaternion qRot;
        const btTransform *mainTransform;
        btRigidBody* rbody = 0;
        btSoftBody* sbody = 0;

        // loop through all bodies in the cens_physics world
        for (int i = 0; i < m_phDynamicsWorld->getNumCollisionObjects(); i++ ) 
        {

            btCollisionObject*	colObj=m_phDynamicsWorld->getCollisionObjectArray()[i];
            if( colObj->getInternalType() == btCollisionObject::CO_RIGID_BODY ) {

                rbody = btRigidBody::upcast(colObj);

                ////////////////////////////////////
                // get the body current transform //

                mainTransform = &(colObj->getWorldTransform());

                mainTransform->getOpenGLMatrix( m );
                qRot = mainTransform->getRotation();
                rot = mainTransform->getBasis();
                orig = mainTransform->getOrigin();


                Vector3f glorg = btVec2eigen(orig);
                Matrix3f glrot = btMat2eigen(rot);

                m_eShapes[colObj]->draw( glorg, glrot );

            }
            else if ( colObj->getInternalType() == btCollisionObject::CO_SOFT_BODY ) 
            {

                sbody = btSoftBody::upcast(colObj);

                ////////////////////////////////////
                // get the body current transform //

                mainTransform = &(getTransformFromBody(sbody)); 

                mainTransform->getOpenGLMatrix( m );
                qRot = mainTransform->getRotation();
                rot = mainTransform->getBasis();
                orig = mainTransform->getOrigin();

                for(int n=0; n<sbody->m_faces.size(); n++) 
                {

                    btSoftBody::Node* node_0=sbody->m_faces[n].m_n[0];
                    btSoftBody::Node* node_1=sbody->m_faces[n].m_n[1];
                    btSoftBody::Node* node_2=sbody->m_faces[n].m_n[2];

                    m_grShapes[i]->getVertices()[n*3 +0] = btVec2eigen(node_0->m_x); 
                    m_grShapes[i]->getVertices()[n*3 +1] = btVec2eigen(node_1->m_x); 
                    m_grShapes[i]->getVertices()[n*3 +2] = btVec2eigen(node_2->m_x); 
                    m_grShapes[i]->getNormals()[n] = btVec2eigen(node_2->m_n); 

                }

                m_eShapes[colObj]->drawSoft();

            }

            ////////////////////////////////////
            // draw object axes ////////////////
            if(m_grObjectAxesEnabled)
                drawAxis( btVec2eigen(orig), btMat2eigen(rot), 40 );

        }

        ////////////////////////////////////
        // draw axis ///////////////////////

        if(m_grAxisEnabled) 
        { 

            Matrix3f id = Matrix3f::Identity();
            drawAxis(Vector3f(0,0,0),  id, 80);

        }

        ////////////////////////////////////
        // draw camera axis ////////////////

        for(unsigned int index=0; index<m_eAttachedCameras.size(); index++) 
        {

            if(m_grCameraAxisEnabled) { 

                CENSBodyCameraData &data =m_eAttachedCameras[index];  
                CENSCamera &camera = *m_grEyeCameras[data.m_bcIdx];
                btTransform &cameraTransform = getTransformFromBody(data.m_bcBody);

                drawLine( 
                        camera.m_cOrigin,
                        btVec2eigen(
                            cameraTransform*(
                                data.m_bcLocal_transform.getOrigin() +  
                                data.m_bcLocal_transform.getBasis() * (data.m_bcUp*camera.m_cDistance))),
                        Vector3f(1,1,1) );
                drawLine( camera.m_cOrigin, 
                        camera.m_cTarget, 
                        Vector3f(.6,0,0) );

            } 

            syncAttachedCamera(index);

        }


        ////////////////////////////////////
        // draw joint pivot axis ///////////
        if(m_grJointAxesEnabled) 
        { 

            for (int i = 0; i < m_phDynamicsWorld->getNumConstraints(); i++ ) 
            {

                btTypedConstraint *joint = m_phDynamicsWorld->getConstraint(i);
                btTransform pivot1;
                btTransform pivot2;
                btTransform &t1=joint->getRigidBodyA().getWorldTransform();
                btTransform &t2=joint->getRigidBodyB().getWorldTransform();

                if( joint->getConstraintType() == HINGE_CONSTRAINT_TYPE ) 
                { 
                    pivot1 = t1 *  ((CENSHingeConstraint *)joint)->getAFrame();    
                    drawAxis( btVec2eigen(pivot1.getOrigin()), btMat2eigen(pivot1.getBasis()), 3 );
                    pivot2 = t2 *   ((CENSHingeConstraint *)joint)->getBFrame();     
                    drawAxis( btVec2eigen(pivot2.getOrigin()), btMat2eigen(pivot2.getBasis()), 3 );
                }
                else if( joint->getConstraintType() == CONETWIST_CONSTRAINT_TYPE ) 
                {
                    pivot1 = t1 *  ((btConeTwistConstraint *)joint)->getAFrame();     
                    drawAxis( btVec2eigen(pivot1.getOrigin()), btMat2eigen(pivot1.getBasis()), 3 );
                    pivot2 = t2 *   ((btConeTwistConstraint *)joint)->getBFrame();  
                    drawAxis( btVec2eigen(pivot2.getOrigin()), btMat2eigen(pivot2.getBasis()), 3 );
                }
                else if( joint->getConstraintType() == SLIDER_CONSTRAINT_TYPE ) 
                {
                    pivot1 = t1 *  ((btSliderConstraint *)joint)->getFrameOffsetA();     
                    drawAxis( btVec2eigen(pivot1.getOrigin()), btMat2eigen(pivot1.getBasis()), 3 );
                    pivot2 = t2 *   ((btSliderConstraint *)joint)->getFrameOffsetB();  
                    drawAxis( btVec2eigen(pivot2.getOrigin()), btMat2eigen(pivot2.getBasis()), 3 );
                }
                else if( joint->getConstraintType() == D6_CONSTRAINT_TYPE ) 
                {
                    pivot1 = t1 *  ((btGeneric6DofConstraint *)joint)->getFrameOffsetA();     
                    drawAxis( btVec2eigen(pivot1.getOrigin()), btMat2eigen(pivot1.getBasis()), 3 );
                    pivot2 = t2 *   ((btGeneric6DofConstraint *)joint)->getFrameOffsetB();  
                    drawAxis( btVec2eigen(pivot2.getOrigin()), btMat2eigen(pivot2.getBasis()), 3 );
                }
            }

        }

        ////////////////////////////////////

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // BODY BUILDING OVERRIDES ////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    btRigidBody* CENSEngine::localCreateRigidBody(
            float mass, 
            const btTransform& startTransform,
            btCollisionShape* shape,     
            const btVector3 &color_,
            const TexCoords &texCoords,
            CENSPixelMap &pixmap ) 
    {

        btRigidBody *body = CENSPhysics::localCreateRigidBody(mass, startTransform, shape);

        Vector3f color = btVec2eigen(color_);

        bool is_empty_pixmap = pixmap.getData()==0;
        bool is_empty_color = color == CENS_NULL_COLOR;

        switch( shape->getShapeType() ) 
        {

            case STATIC_PLANE_PROXYTYPE:
                { 
                    const btStaticPlaneShape* staticPlaneShape = 
                        static_cast<const btStaticPlaneShape*>(shape);    
                    btScalar planeConst = 
                        staticPlaneShape->getPlaneConstant();    
                    const btVector3& planeNormal = 
                        staticPlaneShape->getPlaneNormal();
                    btVector3 planeOrigin = 
                        planeNormal * planeConst;
                    btVector3 vec0,vec1;
                    btPlaneSpace1(planeNormal,vec0,vec1);

                    Vector3f org = btVec2eigen(planeOrigin);
                    Vector3f v0 = btVec2eigen(vec0);
                    Vector3f v1 = btVec2eigen(vec1);

                    CENSGraphicsShape *gsshape = new CENSGraphicsShape();

                    gsshape->buildAsPlane(org,v0,v1,
                            is_empty_color?m_grBoxColor:color,
                            is_empty_pixmap?CENSPixelMap():pixmap);
                    m_grShapes.push_back(gsshape);

                    m_eShapes[body] = gsshape;
                }
                break;
            case BOX_SHAPE_PROXYTYPE:
                { 
                    const btBoxShape* boxShape = 
                        static_cast<const btBoxShape*>(shape);
                    btVector3 halfExtent = 
                        boxShape->getHalfExtentsWithMargin();
                    Vector3f glHalfExtent = 
                        btVec2eigen(halfExtent);

                    CENSGraphicsShape *gsshape = new CENSGraphicsShape();
                    gsshape->buildAsBox(glHalfExtent,
                            is_empty_color?m_grBoxColor:color,
                            is_empty_pixmap?CENSPixelMap():pixmap);
                    m_grShapes.push_back(gsshape);
            
                    m_eShapes[body] = gsshape;
                }
                break;
            case SPHERE_SHAPE_PROXYTYPE:
                {
                    const btSphereShape* sphereShape = 
                        static_cast<const btSphereShape*>(shape);
                    float radius = sphereShape->getMargin();

                    CENSGraphicsShape *gsshape = new CENSGraphicsShape();
                    gsshape->buildAsSphere(radius,
                            is_empty_color?m_grSphereColor:color,
                            is_empty_pixmap?CENSPixelMap():pixmap);
                    m_grShapes.push_back(gsshape);
            
                    m_eShapes[body] = gsshape;
                }
                break;
            default:
                if( shape->isConvex()) 
                {
                    const btConvexShape* convexShape =
                        static_cast<const btConvexShape*>(shape);

                    btShapeHull hull(convexShape);

                    if(hull.buildHull(convexShape->getMargin())) 
                    {

                        const btVector3* btvtx = hull.getVertexPointer();
                        const unsigned int* idx = hull.getIndexPointer();
                        std::vector<Vector3f> vtx(hull.numVertices()); 

                        for(int x=0; x< hull.numVertices(); x++) 
                        {
                            Vector3f v=btVec2eigen(btvtx[x]);
                            vtx[x]=v;
                        }

                        CENSGraphicsShape *gsshape = new CENSGraphicsShape();
                        gsshape->buildAsConvex( vtx, idx, 
                                hull.numVertices(), 
                                hull.numIndices(), 
                                hull.numTriangles(),
                                texCoords,
                                is_empty_color?m_grCapsuleColor:color,
                                is_empty_pixmap?CENS_NULL_PIXMAP:pixmap);

                        m_grShapes.push_back(gsshape);
               
                        m_eShapes[body] = gsshape;
                    }
                }
                break;
        }

        return body;

    }  

    ///////////////////////////////////////////////////////////////////////////////////////////////
   
    btRigidBody* CENSEngine::localImportRigidBody(
            btRigidBody* body,     
            const btVector3 &color_,
            const TexCoords &texCoords,
            CENSPixelMap &pixmap ) 
    {

        float mass = (body->getInvMass() >0 ) ? 1.0/(body->getInvMass()): 0;
    
        const btTransform& startTransform = body->getCenterOfMassTransform();
            
        btCollisionShape* shape = body->getCollisionShape();

        Vector3f color = btVec2eigen(color_);

        bool is_empty_pixmap = pixmap.getData()==0;
        bool is_empty_color = color == CENS_NULL_COLOR;

        switch( shape->getShapeType() ) 
        {

            case STATIC_PLANE_PROXYTYPE:
                { 
                    const btStaticPlaneShape* staticPlaneShape = 
                        static_cast<const btStaticPlaneShape*>(shape);    
                    btScalar planeConst = 
                        staticPlaneShape->getPlaneConstant();    
                    const btVector3& planeNormal = 
                        staticPlaneShape->getPlaneNormal();
                    btVector3 planeOrigin = 
                        planeNormal * planeConst;
                    btVector3 vec0,vec1;
                    btPlaneSpace1(planeNormal,vec0,vec1);

                    Vector3f org = btVec2eigen(planeOrigin);
                    Vector3f v0 = btVec2eigen(vec0);
                    Vector3f v1 = btVec2eigen(vec1);

                    CENSGraphicsShape *gsshape = new CENSGraphicsShape();

                    gsshape->buildAsPlane(org,v0,v1,
                            is_empty_color?m_grBoxColor:color,
                            is_empty_pixmap?CENSPixelMap():pixmap);
                    m_grShapes.push_back(gsshape);

                    m_eShapes[body] = gsshape;
                }
                break;
            case BOX_SHAPE_PROXYTYPE:
                { 
                    const btBoxShape* boxShape = 
                        static_cast<const btBoxShape*>(shape);
                    btVector3 halfExtent = 
                        boxShape->getHalfExtentsWithMargin();
                    Vector3f glHalfExtent = 
                        btVec2eigen(halfExtent);

                    CENSGraphicsShape *gsshape = new CENSGraphicsShape();
                    gsshape->buildAsBox(glHalfExtent,
                            is_empty_color?m_grBoxColor:color,
                            is_empty_pixmap?CENSPixelMap():pixmap);
                    m_grShapes.push_back(gsshape);
          
                    m_eShapes[body] = gsshape;
                }
                break;
            case SPHERE_SHAPE_PROXYTYPE:
                {
                    const btSphereShape* sphereShape = 
                        static_cast<const btSphereShape*>(shape);
                    float radius = sphereShape->getMargin();

                    CENSGraphicsShape *gsshape = new CENSGraphicsShape();
                    gsshape->buildAsSphere(radius,
                            is_empty_color?m_grSphereColor:color,
                            is_empty_pixmap?CENSPixelMap():pixmap);
                    m_grShapes.push_back(gsshape);
         
                    m_eShapes[body] = gsshape;
                }
                break;
            default:
                if( shape->isConvex()) 
                {
                    const btConvexShape* convexShape =
                        static_cast<const btConvexShape*>(shape);

                    btShapeHull hull(convexShape);

                    if(hull.buildHull(convexShape->getMargin())) 
                    {

                        const btVector3* btvtx = hull.getVertexPointer();
                        const unsigned int* idx = hull.getIndexPointer();
                        std::vector<Vector3f> vtx(hull.numVertices()); 

                        for(int x=0; x< hull.numVertices(); x++) 
                        {
                            Vector3f v=btVec2eigen(btvtx[x]);
                            vtx[x]=v;
                        }

                        CENSGraphicsShape *gsshape = new CENSGraphicsShape();
                        gsshape->buildAsConvex( vtx, idx, 
                                hull.numVertices(), 
                                hull.numIndices(), 
                                hull.numTriangles(),
                                texCoords,
                                is_empty_color?m_grCapsuleColor:color,
                                is_empty_pixmap?CENS_NULL_PIXMAP:pixmap);

                        m_grShapes.push_back(gsshape);

           
                        m_eShapes[body] = gsshape;
                    }
                }
                break;
        }

        return body;

    }  



    ///////////////////////////////////////////////////////////////////////////////////////////////

    btSoftBody* CENSEngine::localCreateSoftBody(
            float mass, 
            btCollisionShape* shape,     
            const btVector3 &color_,
            const TexCoords &texCoords,
            CENSPixelMap &pixmap  ) 
    {

        btSoftBody * body = CENSPhysics::localCreateSoftBody(mass,shape);

        Vector3f color = btVec2eigen(color_);

        bool is_empty_pixmap = pixmap.getData()==0;
        bool is_empty_color = color == CENS_NULL_COLOR;

        CENSGraphicsShape *gsshape = new CENSGraphicsShape();
        gsshape->buildAsSoft(body->m_faces.size(),
                texCoords,
                is_empty_color?m_grSoftColor:color,
                is_empty_pixmap?CENS_NULL_PIXMAP:pixmap);
        m_grShapes.push_back(gsshape); 
      
        m_eShapes[body] = gsshape;

        return body;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    btSoftBody* CENSEngine::localLinkSoftBody(
            btSoftBody* body,
            const btVector3 &color_,
            const TexCoords &texCoords,
            CENSPixelMap &pixmap  )
    {

        Vector3f color = btVec2eigen(color_);

        bool is_empty_pixmap = pixmap.getData()==0;
        bool is_empty_color = color == CENS_NULL_COLOR;

        CENSGraphicsShape *gsshape = new CENSGraphicsShape();
        gsshape->buildAsSoft(body->m_faces.size(),
                texCoords,
                is_empty_color?m_grSoftColor:color,
                is_empty_pixmap?CENS_NULL_PIXMAP:pixmap);
        m_grShapes.push_back(gsshape); 

        m_eShapes[body] = gsshape;

        return body;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // CAMERAS ////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    int CENSEngine::attachCamera( 
            btRigidBody *body,                
            btTransform local_transform,      
            std::string screenTitle,              
            int screenWidth,
            int screenHeight,
            int screenXGap,
            int screenYGap,
            btVector3 up,                               
            float target,            
            float foV,
            float near,
            float far
            ) 
    {


        CENSCamera *pCam = new CENSCamera(m_grViewCamera);

        pCam->m_cHeight=0;    
        pCam->m_cScreenTitle=screenTitle.c_str();              
        pCam->m_cScreenWidth=screenWidth;
        pCam->m_cScreenHeight=screenHeight;
        pCam->m_cScreenXGap=screenXGap;
        pCam->m_cScreenYGap=screenYGap;          
        pCam->m_cDistance=1;  
        pCam->m_cFoV=foV;
        pCam->m_cNear=near;
        pCam->m_cFar=far;
        initCamera(*pCam,m_grLight);


        CENSCamera &camera= *pCam;
        m_grEyeCameras.push_back(&camera);

        int idx = m_grEyeCameras.size()-1;
        CENSBodyCameraData data;
        data.m_bcIdx = idx;
        data.m_bcBody = body;
        data.m_bcOrigin = local_transform.getOrigin();
        data.m_bcTarget = 
            local_transform.getOrigin() +  
            local_transform.getBasis() * btVector3(target,0,0);
        data.m_bcUp =  local_transform.getBasis() * up;


        data.m_bcLocal_transform = local_transform;
        m_eAttachedCameras.push_back( data );

        syncAttachedCamera(idx); 

        return idx;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////

    void CENSEngine::syncAttachedCamera(int index) 
    {

        CENSBodyCameraData data = m_eAttachedCameras[index];
        btRigidBody *body = data.m_bcBody;
        CENSCamera &camera = *m_grEyeCameras[data.m_bcIdx];

        btTransform &mainTransform = getTransformFromBody(body);

        camera.m_cOrigin = 
            btVec2eigen( 
                    mainTransform * data.m_bcOrigin  );

        camera.m_cTarget = 
            btVec2eigen( 
                    mainTransform * data.m_bcTarget );

        camera.m_cUp =  btVec2eigen( 
                mainTransform.getBasis() * data.m_bcUp );

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////
    // Eigen - bt convertions ///////////////////////
    /////////////////////////////////////////////////

    Eigen::Vector3f btVec2eigen(const btVector3& v) 
    {
        return Eigen::Vector3f(v.x(), v.y(), v.z());
    }

    btVector3 eigen2btVec(const Eigen::Vector3f& v) 
    {
        return btVector3(v.x(), v.y(), v.z());
    }

    btMatrix3x3 eigen2btMat(const Eigen::Matrix3f& v) 
    {
        btMatrix3x3 bv;
        for(int x=0;x<3;x++)
            for(int y=0;y<3;y++)
                bv[x][y]=v(x,y);
        return bv;
    }

    Eigen::Matrix3f btMat2eigen(const btMatrix3x3& v) 
    {
        Eigen::Matrix3f bv;
        for(int x=0;x<3;x++)
            for(int y=0;y<3;y++)
                bv(x,y)=v[x][y];
        return bv;
    }

}
