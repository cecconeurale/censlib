// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_types.cpp
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#include "cens_types.h"

namespace cens {

  Vector3f CENS_NULL_COLOR = Vector3f(-1,-1,-1);
  Vertices CENS_NULL_VERTICES = Vertices();
  TexCoords CENS_NULL_TEXCOORDS = TexCoords();
  CENSPixelMap CENS_NULL_PIXMAP = CENSPixelMap();
  strings CENS_NULL_STRINGS = strings();
  
  const std::string CENS_DEFAULT_CONTROL = "CENS_DEFAULT_CONTROL";
  const std::string CENS_PD_CONTROL = "CENS_PD_CONTROL"; 

  const int GS_SPHERE_LATS = 20;
  const int GS_SPHERE_LONGS = 20;
  


}
