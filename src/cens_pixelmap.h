// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_pixelmap.h
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CENS_PIXELMAP_H
#define CENS_PIXELMAP_H

#include "Magick++.h"

using namespace Magick;

namespace cens {

  /**
   * @brief Use a byte vector as a matrix
   *
   * Encapsulates a byte vector so that it can
   * be managed as a matrix given width and height.
   * For instance a 800x600 image stored as a 
   * 480000 bytes vector can be used as a [800,600]
   * matrix.
   */
  class CENSPixelMap {

    public:
      CENSPixelMap():m_pxWidth(0),m_pxHeight(0),isLoaded(false) {};

      CENSPixelMap(const CENSPixelMap &copy) {
        
        m_pxWidth=copy.m_pxWidth; 
        m_pxHeight=copy.m_pxHeight;
        m_pxData=copy.m_pxData;
        isLoaded=copy.isLoaded;

      }

      CENSPixelMap &operator=(const CENSPixelMap &copy) {
        
        m_pxWidth=copy.m_pxWidth; 
        m_pxHeight=copy.m_pxHeight;
        m_pxData=copy.m_pxData;
        isLoaded=copy.isLoaded;

        return *this;
      }

      CENSPixelMap(std::string imgfile) {
        
        Image image(imgfile);
        image.magick( "RGB" );
        image.write( &m_pxData );  
        m_pxWidth=image.columns();
        m_pxHeight=image.rows();
        isLoaded=true;

      }

      ~CENSPixelMap() {
      }

      GLubyte operator()(int row, int col, int channel) {
        return ((GLubyte*) m_pxData.data())[row*m_pxWidth + col*3 + channel];
      }

      void setData(Blob &data, int width, int height) {
        std::cout << "set data" << std::endl;
        isLoaded=true;
        m_pxData=data;
        m_pxWidth=width;
        m_pxHeight=height;
      }
      void setData(GLubyte *pixels, int width, int height) {
        isLoaded=true;
        m_pxData=Blob(pixels,width*height*3);
        m_pxWidth=width;
        m_pxHeight=height;
      }
      const GLubyte *getData() const { 
        if(isLoaded) {
          return (GLubyte *)m_pxData.data();    
        }
        return 0;
      }

      Blob &getBlob() {  return m_pxData;  }
      int getWidth() const { return m_pxWidth; } 
      int getHeight() const { return m_pxHeight; }


    private:

      Blob m_pxData;
      int m_pxWidth; 
      int m_pxHeight;
      bool isLoaded;

  };

}

#endif //CENS_PIXELMAP_H
