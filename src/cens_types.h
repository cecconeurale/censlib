// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_types.h
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CENS_TYPES_H
#define CENS_TYPES_H

#include <vector>

// linear algebra
#include <Eigen/Core>
#include <Eigen/Geometry>


using namespace Eigen;

// OpenGL
#include <GL/glut.h>
#include <GL/freeglut.h>

#include "cens_pixelmap.h"


namespace cens {

  typedef std::vector<Vector3f> Vertices;
  typedef std::vector<Vector2f> TexCoords;
  typedef std::vector<std::string> strings;
  typedef std::vector<int> Integers;
  typedef std::vector<float> Floats;

  extern Vector3f CENS_NULL_COLOR;
  extern Vertices CENS_NULL_VERTICES;
  extern TexCoords CENS_NULL_TEXCOORDS;
  extern CENSPixelMap CENS_NULL_PIXMAP;
  extern strings CENS_NULL_STRINGS;
  
  enum CENSConstraint { 
    CENS_FIXED_CONSTRAINT,
    CENS_HINGE_CONSTRAINT,
    CENS_CONETWIST_CONSTRAINT, 
    CENS_SLIDER_CONSTRAINT 
  };

  extern const std::string CENS_DEFAULT_CONTROL;
  extern const std::string CENS_PD_CONTROL; 
  
  enum GS_TYPE {
    GS_NULL = 1,
    GS_PLANE,
    GS_BOX,
    GS_SPHERE,
    GS_CONVEX,
    GS_SOFT 
  };

  extern const int GS_SPHERE_LATS;
  extern const int GS_SPHERE_LONGS;

}

#endif // CENS_TYPES_H
