// Computational Embodied Neuroscience Simulator (CENS) Library
// Copyright (c) 2010 Francesco Mannella
//
// cens_graphics.h
// Copyright (c) 2010 Francesco Mannella
//
// This file is part of CENS library.
//
// CENS library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CENS library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CENS library.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CENS_GRAPHICS_H
#define CENS_GRAPHICS_H
#include <vector>

// linear algebra
#include <Eigen/Core>
#include <Eigen/Geometry>
using namespace Eigen;


// OpenGL
#include <GL/glut.h>
#include <GL/freeglut.h>

//ImageMagick
#include <Magick++.h>

#include "cens_graphics_shape.h"
#include "cens_pixelmap.h"
#include "cens_parameter_manager.h"

static const char* graph_params_filename = "graphics_parameters";

namespace cens 
{

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief A C++ wrapper for OpenGL. 
     * 
     * It allows to use graphics easily, 
     * encapsulating low-level particulars.
     * It uses the Glut interface
     * to OpenGL.
     * General parameters can be changed 
     * by modifing the "graphics_parameters" 
     * file that is created the first time the 
     * application is called.
     * An object of this class must be
     * passed to the  CENSGraphics::cens_graphics pointer
     * so that all callbacks 
     * can be linked and used in the right way:
     *
     \verbatim  

     #include "cens_graphics.h"
     ...
     CENSGraphics mygraph(...); 
     cens_graphics = &mygraph;

     \endverbatim
     */

    class CENSGraphics 
    {

        public: 

            ///////////////////////////////////////////////////////////////// 
            // CONSTRUCTORS /////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////

            CENSGraphics():m_grParameterManager(graph_params_filename)           
            {

                m_grTextureEnabled=false;
                m_grTextureInitialized=false;
                m_grAxisEnabled=false;
                m_grSimEnabled=false;
                m_grCameraAxisEnabled=false;
                m_grObjectAxesEnabled=false;
                m_grJointAxesEnabled=false;

            };

            ~CENSGraphics();

            ///////////////////////////////////////////////////////////////// 
            // PUBLIC METHODS ///////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////

            /** Initialize cens_graphics. */
            virtual void initCENSGraphics( int argc, char ** argv );

            /** Calls the cens_graphics loop function. */
            virtual void loop(); 

            /** Calls the cens_graphics loppevent function */
            virtual void stepToStepLoop();

            /** One step of cens_graphics rendering. */    
            virtual void step( 
                    int ts  /**< time-step. A timer recalls the cens_graphics step function every ts time. */ );

            /** Rendering of all objects */     
            virtual void display();

            /** Stop screen visualization. */         
            virtual bool isIdle();

            /** Exit simulation. */         
            virtual void quit();

            /** Process keyboard shortcuts. */         
            virtual void keyboard( unsigned char key, int x, int y );

        protected: 

            ///////////////////////////////////////////////////////////////// 
            // PROTECTED STRUCTS ////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////

            /** 
             * @brief parameters of a camera
             */
            struct CENSCamera 
            {

                CENSCamera() {}

                CENSCamera(const CENSCamera& copy) 
                {
                    m_cHandle = copy.m_cHandle;
                    m_cScreenWidth = copy.m_cScreenWidth;
                    m_cScreenHeight = copy.m_cScreenHeight;
                    m_cScreenXGap = copy.m_cScreenXGap;
                    m_cScreenYGap = copy.m_cScreenYGap;
                    m_cScreenTitle = copy.m_cScreenTitle;
                    m_cFoV = copy.m_cFoV;
                    m_cRatio = copy.m_cRatio;
                    m_cNear = copy.m_cNear;
                    m_cFar = copy.m_cFar;
                    m_cAngle = copy.m_cAngle;
                    m_cDistance = copy.m_cDistance;
                    m_cHeight = copy.m_cHeight;
                    m_cOrigin = copy.m_cOrigin;
                    m_cTarget = copy.m_cTarget;
                    m_cUp = copy.m_cUp;
                    m_cMov = copy.m_cMov;
                    m_cGap = copy.m_cGap;
                }

                ~CENSCamera() 
                { 

                }

                /** 
                 * Initialize the pixel 
                 * storage array, an array
                 * of unsigned charsin which every 
                 * pixel is represented by three 
                 * subsequents unsigned chars
                 */
                void initPixels() {

                    int data_length =
                        m_cScreenWidth * 
                        m_cScreenHeight * 3 ;
                    unsigned char *pixels = 
                        new unsigned char[ data_length ];

                    m_cPixmap=CENSPixelMap();
                    m_cPixmap.setData(pixels,
                            m_cScreenWidth,m_cScreenHeight);

                    delete pixels;

                }

                /** handle of the camera */
                int m_cHandle;
                /** pixel width of the camera */      
                int m_cScreenWidth;
                /** pixel height of the camera */      
                int m_cScreenHeight;
                /** 
                 * horizontal position
                 * of the camera from the 
                 * left of the screen 
                 */      
                int m_cScreenXGap;
                /** 
                 * vertical position
                 * of the camera from the 
                 * top of the screen 
                 */   
                int m_cScreenYGap;
                /** 
                 * title of the camera
                 */ 
                std::string m_cScreenTitle;
                /** field of view */
                float m_cFoV;       
                /** ratio of the camera field of view */
                float m_cRatio;
                /** distance of the near clipping plane */
                float m_cNear;
                /** distance of the far clipping plane */
                float m_cFar;
                /** angle of view */      
                float m_cAngle;
                /** distance from target */      
                float m_cDistance;
                /** height relative to the target */      
                float m_cHeight;
                /** position of the camera */      
                Vector3f m_cOrigin;
                /** position of the target */      
                Vector3f m_cTarget;
                /** up vector */      
                Vector3f m_cUp;
                /** 
                 * camera rotation in 
                 * response to a single 
                 * keyboard command 
                 */      
                float m_cMov;
                /** 
                 * camera up/down movement 
                 * in response to a single 
                 * keyboard command 
                 * */  
                float m_cGap;
                /** pixel storage array */
                unsigned char *m_cPixels;
                /** 
                 * pixel storage array 
                 * interface for 
                 * 2D-reading of the 
                 * pixel space
                 */
                CENSPixelMap m_cPixmap;

            };

            /** 
             * @brief Light settings of the scene 
             */
            struct CENSLight 
            {

                /** ambient RGBA intensity of the light */
                Vector4f m_lAmbient;
                /** diffuse RGBA intensity of the light */
                Vector4f m_lDiffuse;
                /** specular RGBA intensity of the light */
                Vector4f m_lSpecular;
                /** position of the first light in homogeneous object coordinates */
                Vector4f m_lPosition0;
                /** position of the second light in homogeneous object coordinates */
                Vector4f m_lPosition1;

            };

            ///////////////////////////////////////////////////////////////// 
            // PROTECTED METHODS ////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////

            // SETTINGS /////////////////////////////////////////////////////   

            /** Begin a rendering session. */           
            void beginRendering();

            /** End a rendering session. */               
            void endRendering();

            /** Initialize texturing. */                   
            void initTextures();

            /** Initialize texturing without graphics_shape objects. */                   
            void initTexture(const GLubyte *source = 0, int width = 0, int height = 0);

        public: 

            // CAMERA ///////////////////////////////////////////////////////

            /** Init camera viewpoint. */                   
            void initCamera(CENSCamera &camera, CENSLight light);

            /** Update camera viewpoint. */                   
            void focusCamera(int camera_index);

            /** Update camera viewpoint. */                   
            void updateCamera(int camera_index);

            /** get CENSCamera pixels*/
            const CENSPixelMap &getCameraPixelMap(int camera_index );

            /** save CENSCamera pixels*/
            void saveCameraPixelMap(int camera_index, int t, const char * type="jpg" );

            // RENDERING ////////////////////////////////////////////////////

            /** Draw a line */
            void drawLine(
                    const Vector3f &v1,
                    const Vector3f &v2,
                    const Vector3f color);

            /** Draw a plane or half-empty space. Three perpendicular      
             * versors are given as parameters, indicating the 
             * sense of the empty space. */                       
            void drawPlane( 
                    const Vector3f& orig,    /**< a versor indicating the direction of the empty part of the space. */
                    const Vector3f& vec0,    /**< the second versor, perpendicular to orig.  */
                    const Vector3f& vec1     /**< the third versor, perpendicular to both orig and vec0.  */ );

            /** Draw a box. */
            void drawBox( 
                    const Vector3f& orig,        /**< origin. */
                    const Matrix3f& rotMat,      /**< transformation.  */
                    const Vector3f& halfExtent   /**< half extents with margin. */ );

            /** Draw a sphere. */
            void drawSphere( 
                    const Vector3f& orig,    /**< origin. */
                    const Matrix3f& rot,     /**< rotation. */
                    float radius             /**< radius. */ );
                    
            /** Draw a cens convex object. */
            void drawConvex( 
                    const Vector3f& orig,               /**< origin. */
                    const Matrix3f& rot ,               /**< rotation. */
                    const std::vector<Vector3f> &vtx,   /**< list of triangular vertexes. */ 
                    const unsigned int *idx,            /**< list of indices of triangular vertexes. */
                    int nvtxs,                          /**< number of triangular vertexes. */
                    int nidxs,                          /**< number indices of  triangular vertexes. */
                    int ntrns                           /**< number indices of  triangles. */ );  

            /** Draw XYZ axes. */
            void drawAxis(
                    const Vector3f &origin,             /**< origin. */
                    const Matrix3f &rotation,           /**< rotation. */
                    float length                        /**< length of each axis. */);

            /** Draw a vertex. */
            inline void drawVector( const Vector3f& v /**< vertex. */ ) 
            { 
                glVertex3d(v[0], v[1], v[2]); 
            }

            /** Draw a triangle. */
            void drawTriangle(
                    const Vector3f &v1,     /**< first vertex. */
                    const Vector3f &v2,     /**< second vertex. */
                    const Vector3f &v3,     /**< third vertex. */
                    const Vector3f n        /**< normal. */ );

            // KEYBOARD /////////////////////////////////////////////////////

            void stepLeft();          /**< Turn to the left. */
            void stepRight();         /**< Turn to the right. */
            void stepFront();         /**< Go up. */
            void stepBack();          /**< Go down. */
            void zoomIn();            /**< Zoom in. */
            void zoomOut();           /**< Zoom out. */
            void toggleStop();        /**< Stop/restart the simulation. */
            void toggleTexture();     /**< Toggle texture. */
            void toggleAxis();        /**< Toggle axis. */
            void toggleObjectAxes();  /**< Toggle object axes. */ 
            void toggleJointAxes();   /**< Toggle joint axes. */ 
            void toggleCameraAxis();  /**< Toggle camera axis. */ 

        protected:

            ///////////////////////////////////////////////////////////////// 
            // PROTECTED MEMBERS ////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////

            /** m_grTimestep of the graphic simulation */
            int m_grTimestep;
            /** program number of arguments */
            int m_grArgc;
            /** list of arguments */
            char ** m_grArgv;

            /** environment color */
            Vector3f m_grEnvColor;
            /** default ground color */
            Vector3f m_grGroundColor;
            /** default object color */
            Vector3f m_grObjectColor;
            /** default box color */
            Vector3f m_grBoxColor;
            /** default capsule color */
            Vector3f m_grCapsuleColor;
            /** default sphere color */
            Vector3f m_grSphereColor;
            /** default soft color */
            Vector3f m_grSoftColor;
            /** default compound color */
            Vector3f m_grCompoundColor;

            /** texture-enabled flag */
            bool m_grTextureEnabled;
            /** texture-initialized flag */
            bool m_grTextureInitialized;

            /** hiding-cens_graphics flag */    
            bool m_grHide;
            /** light parameters structure */        
            CENSLight m_grLight;

            /** axis-enabled flag */    
            bool m_grAxisEnabled;
            /** simulation-enabled flag */    
            bool m_grSimEnabled;
            /** camera-axis-enabled flag */    
            bool m_grCameraAxisEnabled;
            /** object-axes-enabled flag */    
            bool m_grObjectAxesEnabled;
            /** axis-enabled flag */    
            bool m_grJointAxesEnabled;

            /** main camera */   
            CENSCamera m_grViewCamera;
            /** number of supplementar cameras */   
            int m_grEyeCameraNumber;
            /** vector of supplementar cameras */   
            std::vector< CENSCamera *> m_grEyeCameras;
            /** vector of all cameras */   
            std::vector< CENSCamera *> m_grCameras;
            /** vector of graphic shape objects */   
            std::vector< CENSGraphicsShape *> m_grShapes;

            /** Parameter manager */
            CENSParameterManager m_grParameterManager;
    };



    /** static CENSGraphics pointer. */
    extern CENSGraphics* cens_graphics; 

}

#endif // CENS_GRAPHICS_H)
