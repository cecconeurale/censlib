#!/bin/bash

major=$1
minor=$2

perl -pi -e"s/\((\s*)CENS_VERSION_MAJOR \d+(\s*)\)/( CENS_VERSION_MAJOR $major )/" CMakeLists.txt
perl -pi -e"s/\((\s*)CENS_VERSION_MINOR \d+(\s*)\)/( CENS_VERSION_MINOR $minor )/" CMakeLists.txt
perl -pi -e"s/()(.*)\[\d\.\d\.\d\]/\1\2\[0\.$major\.$minor\]/" configure.ac  
perl -pi -e"s/(AC_SUBST)(.*)\[\d:\d:\d\]/\1\2\[0:$major:$minor\]/" configure.ac 
perl -pi -e"s/(AC_SUBST)(.*)\[\d\.\d\]/\1\2\[$major\.$minor\]/" configure.ac 

